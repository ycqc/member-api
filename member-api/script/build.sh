#!/bin/bash
echo "开始编译打包"

cd ../../
echo "编译程序"
gradle trilobite-file-api:buildDependent
cd trilobite-file-api/script

sudo cp ../build/libs/trilobite-file-api-1.0.0.jar .
ssh root@47.104.173.124 << remotessh

cd /home
mkdir docker
cd docker
mkdir trilobite-file-api
exit
remotessh
scp ./trilobite-file-api-start.sh root@47.104.173.124:/home/cmd
scp ./Dockerfile root@47.104.173.124:/home/docker/trilobite-file-api
scp ./trilobite-file-api-1.0.0.jar root@47.104.173.124:/home/docker/trilobite-file-api
ssh root@47.104.173.124 << remotessh
cd /home/docker/trilobite-file-api
docker kill file-api
docker rmi trilobite/file-api:1.0.0
docker build -t="trilobite/file-api:1.0.0" .
chmod +x /home/cmd/trilobite-file-api-start.sh
/home/cmd/trilobite-file-api-start.sh
exit
remotessh