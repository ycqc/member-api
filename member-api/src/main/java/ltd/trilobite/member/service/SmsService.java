package ltd.trilobite.member.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import ltd.trilobite.common.RedisTools;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Random;

/**
 * 短消息服务类
 */
public class SmsService {

    /**
     * 发送验证码
     * @param phoneNum
     * @return
     */
    public int sendVcode(String phoneNum){
        DefaultProfile profile = DefaultProfile.getProfile("default", "LTAIecr0Tetvg1gA", "oFkSO0CNDTexas1iEHDSuVWsgHjsQr");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();

        request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");


        int vcode= new Random().nextInt(9999);
        RedisTools.newInstance();
        JedisPool pool = RedisTools.get();
        Jedis jedis = null;
        String key = "vcode."+phoneNum ;

        jedis = pool.getResource();


        //当不存在这个key的时候创建，并发送短消息
        int code=200;
        if(!jedis.exists(key)) {

            System.out.println("验证码是："+vcode+"");
            request.putBodyParameter("PhoneNumbers",phoneNum);
            request.putBodyParameter("SignName","优程");
            request.putBodyParameter("TemplateCode","SMS_165117845");
            request.putBodyParameter("TemplateParam","{\"code\":\""+vcode+"\"}");

            try {
                CommonResponse response = client.getCommonResponse(request);

                JSONObject json= JSON.parseObject(response.getData());
                if(json.get("Message").equals("OK")){
                    jedis.set(key, vcode + "");
                    jedis.expire(key,60);
                    code= 200;
                }else{
                    code= -1;
                }
            } catch (ServerException e) {
                e.printStackTrace();
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }else{
            code=-2; //已成功发送消息，请５分钟后再次发送
        }
        if (!pool.isClosed()) {
            pool.close();
        }

        return code;

    }

    /**
     * 验证验证码是否正确
     * @param phoneNum
     * @param code
     * @return
     */
    public int valiCode(String phoneNum,String code){
        RedisTools.newInstance();
        JedisPool pool = RedisTools.get();
        Jedis jedis = null;
        String key = "vcode." +phoneNum ;

        jedis = pool.getResource();
        int r=200;
        if(jedis.exists(key)&&jedis.get(key).equals(code)){
            r=200;
        }else{
            r=-1;
        }
        if (!pool.isClosed()) {
            pool.close();
        }
        return r;
    }
}
