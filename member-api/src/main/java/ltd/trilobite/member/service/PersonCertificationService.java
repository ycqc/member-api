package ltd.trilobite.member.service;

import ltd.trilobite.member.common.CertificationConstant;
import ltd.trilobite.member.dao.PersonCertificationDao;
import ltd.trilobite.member.dao.entry.PersonCertification;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Map;

public class PersonCertificationService {
    private static final Result SUCCESS = new Result(200, 1, true);
    private static final Result FAIL = new Result(-1, -1, true);
    private static final Logger LOGGER = LogManager.getLogger(PersonCertificationService.class);
    private PersonCertificationDao personCertificationDao = new PersonCertificationDao();

    /**
     * 根据用户ID查询实名认证信息
     *
     * @param personId
     * @return
     */
    public Map<String, Object> findByPersonId(Long personId) {
        return personCertificationDao.findByPersonId(personId);
    }

    /**
     * 根据用户ID查询实名认证信息
     *
     * @param personId
     * @return
     */
    public PersonCertification findFullByPersonId(Long personId) {
        return personCertificationDao.findById(personId);
    }

    /**
     * 更新是否审核通过
     *
     * @param personCertification
     * @param isAudit
     */
    public void updateIsAudit(PersonCertification personCertification, Integer isAudit) {
        personCertification.setIsAudit(isAudit);
        personCertification.setUpdateTime(new Date());
        personCertification.setAuditPerson(personCertification.getAuditPerson());
        personCertificationDao.update(personCertification);
    }

    /**
     * 更新审核状态
     *
     * @param personCertification
     * @param auditState
     */
    public void updateAuditState(PersonCertification personCertification, Integer auditState) {
        personCertification.setAuditState(auditState);
        personCertification.setUpdateTime(new Date());
        personCertification.setAuditPerson(personCertification.getAuditPerson());
        personCertificationDao.update(personCertification);
    }

    /**
     * 新增或更新实名认证信息
     *
     * @param personCertification
     * @return
     */
    public Result save(PersonCertification personCertification) {
        LOGGER.info("新增实名认证");
        PersonCertification queryPersonCertification = findFullByPersonId(personCertification.getPersonId());

        if (queryPersonCertification == null) {
            personCertification.setIsAudit(CertificationConstant.AUDIT_FAIL);
            personCertification.setAuditState(CertificationConstant.STATE_UNCHECK);
            personCertification.setCreateTime(new Date());
            personCertification.setPersonId(personCertification.getPersonId());
            personCertificationDao.add(personCertification);
            LOGGER.info("新增实名认证信息成功");
            return SUCCESS;
        }

        //审核状态为失败时，才能更新实名认证信息
        if (CertificationConstant.AUDIT_FAIL == queryPersonCertification.getIsAudit()) {
            queryPersonCertification.setRealName(personCertification.getRealName());
            queryPersonCertification.setCardFront(personCertification.getCardFront());
            queryPersonCertification.setCardBg(personCertification.getCardBg());
            queryPersonCertification.setHandImg(personCertification.getHandImg());
            updateAuditState(queryPersonCertification, CertificationConstant.STATE_UNCHECK);
            LOGGER.info("更新实名认证信息成功");
            return SUCCESS;
        }

        LOGGER.error("审核状态错误");
        return FAIL;
    }

    /**
     * 设置审核中
     *
     * @param personCertification
     * @return
     */
    public Result setAuditStateToChecking(PersonCertification personCertification) {
        Integer currentAuditState = personCertificationDao.findAuditStateByPersonId(personCertification.getPersonId());
        //审核状态为待审核，才能设置审核中
        if (currentAuditState == CertificationConstant.STATE_UNCHECK) {
            updateAuditState(personCertification, CertificationConstant.STATE_CHECKING);
            LOGGER.info("设置审核中");
            return SUCCESS;
        }

        LOGGER.error("审核状态错误");
        return FAIL;
    }

    /**
     * 设置已完成
     *
     * @param personCertification
     * @return
     */
    public Result setAuditStateToComplete(PersonCertification personCertification) {
        PersonCertification query = personCertificationDao.findById(personCertification.getPersonId());
        //审核状态为待审核，才能设置审核中
        if (query.getIsAudit() == CertificationConstant.AUDIT_OK) {
            updateAuditState(personCertification, CertificationConstant.STATE_CHECKED);
            LOGGER.info("设置审核完成");
            return SUCCESS;
        }

        LOGGER.error("审核状态错误");
        return FAIL;
    }

    /**
     * 审核通过
     *
     * @param personCertification
     * @return
     */
    public Result auditOk(PersonCertification personCertification) {
        PersonCertification queryPersonCertification = findFullByPersonId(personCertification.getPersonId());

        //审核状态为待审核或审核中，才能审核通过
        if (queryPersonCertification.getAuditState() != CertificationConstant.STATE_CHECKED) {
            queryPersonCertification.setCardNo(personCertification.getCardNo());
            queryPersonCertification.setProvince(personCertification.getProvince());
            queryPersonCertification.setCity(personCertification.getCity());
            queryPersonCertification.setArea(personCertification.getArea());
            queryPersonCertification.setStreet(personCertification.getStreet());
            queryPersonCertification.setAuditPerson(personCertification.getPersonId());
            updateIsAudit(queryPersonCertification, CertificationConstant.AUDIT_OK);
            LOGGER.info("审核通过");
            return SUCCESS;
        }

        LOGGER.error("审核状态错误");
        return FAIL;
    }

    /**
     * 审核未通过
     *
     * @param personCertification
     * @return
     */
    public Result auditFail(PersonCertification personCertification) {
        PersonCertification queryPersonCertification = findFullByPersonId(personCertification.getPersonId());

        //审核状态为待审核或审核中，才能审核未通过
        if (queryPersonCertification.getAuditState() != CertificationConstant.STATE_CHECKED) {
            queryPersonCertification.setAuditMessage(personCertification.getAuditMessage());
            queryPersonCertification.setAuditPerson(personCertification.getPersonId());
            updateIsAudit(queryPersonCertification, CertificationConstant.AUDIT_FAIL);
            personCertificationDao.update(queryPersonCertification);
            LOGGER.info("审核未通过");
            return SUCCESS;
        }

        LOGGER.error("审核状态错误");
        return FAIL;
    }

    /**
     * 分页查询实名认证信息
     * @param form
     * @return
     */
    public Result findNaviList(RestForm form) {
        return  personCertificationDao.findNaviList(form);
    }
}
