package ltd.trilobite.member.service;

import ltd.trilobite.member.dao.*;
import ltd.trilobite.member.dao.entry.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * 订单服务
 */
public class OrderService {

    Logger logs = LogManager.getLogger(OrderService.class);
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BillDao billDao = new BillDao();
    MemberStatusDao memberStatusDao = new MemberStatusDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();
    MemberProductDao memberProductDao = new MemberProductDao();
    PersonAchievementsDao personAchievementsDao = new PersonAchievementsDao();
    PerformanceDifferConfigDao performanceDifferConfigDao = new PerformanceDifferConfigDao();


    public void timelyRebate(MemberOrder order) {
        //当订单审核通过后执行
        if (order.getOrderState() == 3) {
            List<Object> list = new ArrayList<>();
            rechargeRebate(order, list);
            recommendationRebate(order, list);
            LayerRebate(order, list);
            achievements(order, list);
            billDao.adds(list);
        }
    }

    /**
     * 订单审核完成后执行
     * 推荐返利
     *
     * @return 200成功
     */
    private void recommendationRebate(MemberOrder order, List<Object> list) {
        logs.info("推荐返利开始");


        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("2");//设置推荐返利
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        MemberStatus msparam = new MemberStatus();
        msparam.setMemberId(order.getPersonId());
        logs.info("查找父亲人员编号");
        MemberStatus parentMemer = memberStatusDao.findOne(msparam, MemberStatus.class);
        billDao.delActionSrcId(order.getOrderId(), "4");

        for (GiveGoodsConfig givegood : givegoods) {
            Bill bill = new Bill();
            bill.setNum(givegood.getNum());
            bill.setCode(givegood.getGoodsCode());
            bill.setPersonId(parentMemer.getParentId());
            bill.setState(1);
            bill.setSrcId(order.getOrderId());
            bill.setDescription("推荐获取礼包");
            bill.setActionCode("4");
            list.add(bill);
        }

    }

    /**
     * 订单审核完成后执行
     * 管理奖返利
     *
     * @return 200成功
     */
    private void LayerRebate(MemberOrder order, List<Object> list) {

        GlobalSet globalsetParam = new GlobalSet();
        globalsetParam.setGlobalSetId(2);
        GlobalSet globleset = globalSetDao.findOne(globalsetParam, GlobalSet.class);

        PersonLayer param = new PersonLayer();
        param.setMemberId(order.getPersonId());
        param.setDepth(Integer.parseInt(globleset.getConstVal()));

        List<PersonLayer> layers = memberStatusDao.layerParent(param);
        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("3");
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        billDao.delActionSrcId(order.getOrderId(), "2");
        for (PersonLayer layer : layers) {
            for (GiveGoodsConfig givegood : givegoods) {
                Bill bill = new Bill();
                bill.setNum(givegood.getNum());
                bill.setCode(givegood.getGoodsCode());
                bill.setPersonId(layer.getParentId());
                bill.setState(1);
                bill.setSrcId(order.getOrderId());
                bill.setActionCode("2");
                bill.setDescription("管理提成,当前处于" + layer.getDepth() + "深度");
                list.add(bill);
            }

        }

    }


    /**
     * 订单审核完成后执行
     * 充值返利
     *
     * @return 200成功
     */
    private void rechargeRebate(MemberOrder order, List<Object> list) {
        logs.info("充值返利");
        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(order.getMemberProductId());
        giveGoodsConfig.setCode("1");//设置充值返利
        List<GiveGoodsConfig> givegoods = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        billDao.delActionSrcId(order.getOrderId(), "3");
        for (GiveGoodsConfig givegood : givegoods) {
            Bill bill = new Bill();
            bill.setNum(givegood.getNum());
            bill.setCode(givegood.getGoodsCode());
            bill.setPersonId(order.getPersonId());
            bill.setState(1);
            bill.setSrcId(order.getOrderId());
            bill.setActionCode("3");
            bill.setDescription("充值礼包");
            list.add(bill);
        }

    }

    /**
     * 计算业绩
     */
    private void achievements(MemberOrder order, List<Object> list) {
        MemberProduct memberProductParam = new MemberProduct();
        memberProductParam.setMemberProductId(order.getMemberProductId());
        MemberProduct memberProduct = memberProductDao.findOne(memberProductParam, MemberProduct.class);
        PersonLayer param = new PersonLayer();
        param.setMemberId(order.getPersonId());
        personAchievementsDao.delForSrcId(order.getOrderId());
        List<PersonLayer> layers = memberStatusDao.layerParentNoDepth(param);

        for (PersonLayer layer : layers) {

            PersonAchievements personAchievements = new PersonAchievements();
            personAchievements.setOrderId(order.getOrderId());
            personAchievements.setOrderPersonId(order.getPersonId());
            personAchievements.setPersonId(layer.getParentId());
            personAchievements.setCreateTime(order.getCreateTime());
            personAchievements.setNum(memberProduct.getPrice());
            list.add(personAchievements);
        }
    }


    /**
     * 绩差奖励
     */
    public void performanceDifference(AchievementsSettlement achievementsSettlement) {
        logs.info("开始计算绩差奖励");
        logs.info("正在通过全局设置中获取全局配置");
        GlobalSet globalsetParam = new GlobalSet();
        globalsetParam.setGlobalSetId(1);
        GlobalSet globleset = globalSetDao.findOne(globalsetParam, GlobalSet.class);
        logs.info("读取绩差配置完成，每单固定按照" + globleset.getConstVal());
        Long pv = Long.parseLong(globleset.getConstVal());
        List<PersonAchievements> personAchievements = personAchievementsDao.findNewAdd(achievementsSettlement.getStartTime(), achievementsSettlement.getEndTime());

        logs.info("读取" + new Date(achievementsSettlement.getStartTime().getTime()) + "到" + new Date(achievementsSettlement.getStartTime().getTime()) + "的新增业绩");

        //极差配置
        PerformanceDifferConfig pdconfigParam = new PerformanceDifferConfig();
        List<PerformanceDifferConfig> performanceDifferConfis = performanceDifferConfigDao.list(pdconfigParam, PerformanceDifferConfig.class);

        String newaddPerson = "";
        int i = 0;
        Map<Long, List<Long>> orderPerson = new HashMap<>();
        Map<Long, Long> orderBuied = new HashMap<>();
        for (PersonAchievements personAddNew : personAchievements) {
            if (!orderPerson.containsKey(personAddNew.getOrderId())) {
                orderPerson.put(personAddNew.getOrderId(), new ArrayList<>());
            }
            if (!orderBuied.containsKey(personAddNew.getOrderId())) {
                orderBuied.put(personAddNew.getOrderId(), personAddNew.getOrderPersonId());
            }
            orderPerson.get(personAddNew.getOrderId()).add(personAddNew.getPersonId());
            if (i > 0) {
                newaddPerson += ",";
            }
            newaddPerson += personAddNew.getPersonId().toString();
            i++;
        }

        Map<Long, Long> team = personAchievementsDao.findTem(achievementsSettlement.getEndTime(), newaddPerson);
        logs.info("团队业绩情况" + team);
        logs.info("参与新业绩分配的所有人" + newaddPerson);
        List<Object> list = new ArrayList<>();
        for (Map.Entry en : orderBuied.entrySet()) {
            logs.info("订单:" + en.getKey());
            logs.info("购买人:" + en.getValue());
            billDao.delActionSrcId((Long) en.getKey(), "1");
            PersonLayer param = new PersonLayer();
            param.setMemberId((Long) en.getValue());
            List<PersonLayer> layers = memberStatusDao.layerParentNoDepth(param);
            int savep = 0;
            for (PersonLayer layer : layers) {
                logs.info("深度" + layer.getDepth());
                logs.info("人员" + layer.getParentId());
                logs.info("团队业绩" + team.get(layer.getParentId()));
                PerformanceDifferConfig conf = checkConfig(team.get(layer.getParentId()), performanceDifferConfis);

                if (conf != null) {
                    logs.info(conf.getLevelNum());
                    logs.info(conf.getAchievement());
                    if (conf.getProportion() > savep) {


                        Bill bill = new Bill();

                        int lastp = savep;
                        savep = conf.getProportion() - savep;
                        bill.setNum(pv * savep / 100d);
                        bill.setCode("5");
                        bill.setPersonId(layer.getParentId());
                        bill.setState(1);
                        bill.setSrcId((Long) en.getKey());
                        bill.setActionCode("1");
                        if (lastp == 0) {
                            bill.setDescription("[业绩奖励]无业绩分出,你的业绩比例为：" + conf.getProportion() + "%，实际比例为：" + savep + "%");

                        } else {
                            bill.setDescription("[业绩奖励]已有业绩：" + lastp + "%已分出,你的业绩比例为：" + conf.getProportion() + "%，实际比例为：" + savep + "%");
                        }
                        list.add(bill);

                    } else {
                        logs.info("平级，无法拿到业绩");
                    }

                    logs.info("可拿到比例" + conf.getProportion() + ":" + savep);
                } else {
                    logs.info("未达到要求，无法拿到业绩");
                }
            }

        }

        billDao.adds(list);

//       for(PersonAchievements personAddNew:personAchievements) {
//           PerformanceDifferConfig conf = checkConfig(team.get(personAddNew.getPersonId()), performanceDifferConfis);
//           if (conf != null) {
//               logs.info("新增业绩来自订单"+personAddNew.getOrderId());
//               logs.info("查看当前人员"+personAddNew.getPersonId()+"新增业绩"+personAddNew.getNum());
//               logs.info(conf.getLevelNum());
//               logs.info(conf.getAchievement());
//               logs.info(conf.getProportion());
//               logs.info("检查下级人员是否和我平级");
//               logs.info("参与人编号"+orderPerson.get(personAddNew.getOrderId()));
//
//           }else{
//               logs.info("未达到级差要求，无法拿到业绩");
//           }
//       }


        //找到新增业绩的团队业绩，以及配置中所属的会员级别

        //找到业绩的人员组织结构关系

        //查找是否下面成员是否给你平级，如果平级就不拿
        //如果未平级，那么查看


    }

    private PerformanceDifferConfig checkConfig(Long num, List<PerformanceDifferConfig> performanceDifferConfis) {
        PerformanceDifferConfig def = null;
        Long min = 0l;
        for (PerformanceDifferConfig performanceDifferConfig : performanceDifferConfis) {

            if (num > performanceDifferConfig.getAchievement()) {
                def = performanceDifferConfig;
                if (performanceDifferConfig.getAchievement() > min) {
                    def = performanceDifferConfig;
                    min = performanceDifferConfig.getAchievement();
                }
            }

        }
        return def;

    }

}
