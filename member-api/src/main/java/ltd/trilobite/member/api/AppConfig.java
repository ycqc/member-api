package ltd.trilobite.member.api;

import io.undertow.server.handlers.resource.PathResourceManager;
import ltd.trilobite.common.RedisTools;
import ltd.trilobite.http.HttpApplication;
import ltd.trilobite.http.HttpPlugins;
import ltd.trilobite.sdk.config.AutoConfig;
import ltd.trilobite.sdk.config.Conf;
import ltd.trilobite.sdk.config.IConfig;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import redis.clients.jedis.Jedis;

import java.nio.file.Paths;

import static io.undertow.Handlers.resource;

@AutoConfig
public class AppConfig implements IConfig {

    @Override
    public void execute() {

        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);
        RedisTools.newInstance();
        Jedis jedis = new Jedis(Conf.get("redis.user.server"), Integer.valueOf(Conf.get("redis.user.port").toString()));
        //jedis.auth(Conf.get("redis.user.password"));

        applicatonFactory.add("redis", jedis);

        HttpPlugins httpPlugins = HttpPlugins.newInstance();
        httpPlugins.add(HttpApplication.getContext() + "/upload", new HttpFileUpload());
        httpPlugins.add(HttpApplication.getContext() + "/img", resource(new PathResourceManager(Paths.get("/opt/upload"), 100)));
//        httpPlugins.add(HttpApplication.getContext() + "/Wx", new WxAction());
        httpPlugins.add(HttpApplication.getContext() + "/vcode", new ValidateCode());
        httpPlugins.add(HttpApplication.getContext() + "/appQrcode", new AppDownLoadQrCode());

    }
}
