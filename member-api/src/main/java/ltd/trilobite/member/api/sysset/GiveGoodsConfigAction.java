package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.GiveGoodsConfigDao;
import ltd.trilobite.member.dao.entry.GiveGoodsConfig;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2006-会员权益")
@RestService
public class GiveGoodsConfigAction {

    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "giveGoodsConfigId", type = "string", desc = "代码"),
            @ApiDesc(name = "memberType", type = "string", desc = "会员类型"),
            @ApiDesc(name = "goodName", type = "string", desc = "物品名称"),
            @ApiDesc(name = "num", type = "string", desc = "数量"),
            @ApiDesc(name = "unit", type = "string", desc = "单位"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/giveGoodsConfig/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param GiveGoodsConfig giveGoodsConfig) {
        giveGoodsConfigDao.add(giveGoodsConfig);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "giveGoodsConfigId", type = "string", desc = "代码"),
            @ApiDesc(name = "memberType", type = "string", desc = "会员类型"),
            @ApiDesc(name = "goodName", type = "string", desc = "物品名称"),
            @ApiDesc(name = "num", type = "string", desc = "数量"),
            @ApiDesc(name = "unit", type = "string", desc = "单位"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/giveGoodsConfig/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param GiveGoodsConfig giveGoodsConfig) {
        giveGoodsConfigDao.update(giveGoodsConfig);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "giveGoodsConfigId", type = "string", desc = "代码"),

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/giveGoodsConfig/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param GiveGoodsConfig giveGoodsConfig) {
        giveGoodsConfigDao.del(giveGoodsConfig);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "giveGoodsConfigId", type = "string", desc = "代码"),
                    @ApiDesc(name = "memberType", type = "string", desc = "会员类型"),
                    @ApiDesc(name = "goodName", type = "string", desc = "物品名称"),
                    @ApiDesc(name = "num", type = "string", desc = "数量"),
                    @ApiDesc(name = "unit", type = "string", desc = "单位"),
            }
    )
    @Router(path = "/sysset/giveGoodsConfig/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param GiveGoodsConfig giveGoodsConfig) {
        return new Result(giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class));
    }
}
