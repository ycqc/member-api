package ltd.trilobite.member.api;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;
import ltd.trilobite.common.QRCodeUtil;

import java.io.OutputStream;


public class AppDownLoadQrCode implements HttpHandler {


    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        httpServerExchange.startBlocking();
        HeaderMap header = httpServerExchange.getResponseHeaders();
        header.add(new HttpString("Pragma"), "no-cache");
        header.add(new HttpString("Cache-Control"), "no-cache");
        header.add(new HttpString("Expires"), -1);
        header.add(new HttpString("Content-Type"), "image/png");
        header.add(new HttpString("Access-Control-Allow-Origin"), "*");

        OutputStream out = httpServerExchange.getOutputStream();

        QRCodeUtil.encode("https://www.baidu.com", out);

    }
}
