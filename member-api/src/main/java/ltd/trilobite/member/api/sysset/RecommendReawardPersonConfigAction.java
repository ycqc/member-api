package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.FunctionDao;
import ltd.trilobite.member.dao.RecommendReawardPersonConfigDao;
import ltd.trilobite.member.dao.entry.Function;
import ltd.trilobite.member.dao.entry.RecommendReawardPersonConfig;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2009-推荐奖人数设置")
@RestService
public class RecommendReawardPersonConfigAction {
    RecommendReawardPersonConfigDao recommendReawardPersonConfigDao = new RecommendReawardPersonConfigDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "recommendReawardPersonConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "personNum", type = "string", desc = "人数"),
            @ApiDesc(name = "layer", type = "string", desc = "可拿层数"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendReawardPersonConfig/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param RecommendReawardPersonConfig recommendReawardPersonConfig) {
        RecommendReawardPersonConfig queryConfig = recommendReawardPersonConfigDao.findById(recommendReawardPersonConfig.getRecommendReawardPersonConfigId());
        if(queryConfig != null){
            return new Result(-1);
        }
        recommendReawardPersonConfigDao.add(recommendReawardPersonConfig);
        return new Result(200);
    }

    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "recommendReawardPersonConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "personNum", type = "string", desc = "人数"),
            @ApiDesc(name = "layer", type = "string", desc = "可拿层数"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendReawardPersonConfig/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param RecommendReawardPersonConfig recommendReawardPersonConfig) {
        recommendReawardPersonConfigDao.update(recommendReawardPersonConfig);
        return new Result(200);
    }


    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "recommendReawardPersonConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendReawardPersonConfig/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        RecommendReawardPersonConfig config = new RecommendReawardPersonConfig();
        config.setRecommendReawardPersonConfigId(Long.parseLong(form.get("recommendReawardPersonConfigId")));
        recommendReawardPersonConfigDao.del(config);
        return new Result(200);
    }

    @ApiDoc(title = "列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "recommendReawardPersonConfigId", type = "string", desc = "编号"),
                    @ApiDesc(name = "personNum", type = "string", desc = "人数"),
                    @ApiDesc(name = "layer", type = "string", desc = "可拿层数"),
            }

    )
    @Router(path = "/sysset/recommendReawardPersonConfig/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return new Result(recommendReawardPersonConfigDao.list(new RecommendReawardPersonConfig(), RecommendReawardPersonConfig.class));
    }
}
