package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.FunctionDao;
import ltd.trilobite.member.dao.entry.Function;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2008-功能管理")
@RestService
public class FunctionAction {
    FunctionDao functionDao = new FunctionDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/function/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param Function function) {
        Function queryFunction = functionDao.findById(function.getFunctionId());
        if(queryFunction != null){
            return new Result(-1);
        }
        functionDao.add(function);
        return new Result(200);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/function/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param Function function) {
        functionDao.update(function);
        return new Result(200);
    }


    @ApiDoc(title = "删除", param = {

            @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/function/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        Function function = new Function();
        function.setFunctionId(Long.parseLong(form.get("functionId")));
        functionDao.del(function);
        return new Result(200);
    }

    @ApiDoc(title = "列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
                    @ApiDesc(name = "name", type = "string", desc = "名称"),
            }

    )
    @Router(path = "/sysset/function/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return new Result(functionDao.list(new Function(), Function.class));
    }
}
