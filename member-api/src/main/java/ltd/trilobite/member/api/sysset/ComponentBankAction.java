package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.ComponentBankDao;
import ltd.trilobite.member.dao.entry.ComponentBank;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2007-公司银行卡")
@RestService
public class ComponentBankAction {

    ComponentBankDao componentBankDao = new ComponentBankDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "componentBankId", type = "string", desc = "编号"),
            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "name", type = "string", desc = "持卡人姓名"),
            @ApiDesc(name = "num", type = "string", desc = "卡号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/componentBank/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param ComponentBank componentBank) {
        componentBankDao.add(componentBank);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "componentBankId", type = "string", desc = "编号"),
            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "name", type = "string", desc = "持卡人姓名"),
            @ApiDesc(name = "num", type = "string", desc = "卡号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/componentBank/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param ComponentBank componentBank) {
        componentBankDao.update(componentBank);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "componentBankId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/componentBank/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param ComponentBank componentBank) {
        componentBankDao.del(componentBank);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "componentBankId", type = "string", desc = "编号"),
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
                    @ApiDesc(name = "name", type = "string", desc = "持卡人姓名"),
                    @ApiDesc(name = "num", type = "string", desc = "卡号"),
            }
    )
    @Router(path = "/sysset/componentBank/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param ComponentBank componentBank) {
        return new Result(componentBankDao.list(componentBank, ComponentBank.class));
    }
}
