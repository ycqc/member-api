package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.PersonFunctionDao;
import ltd.trilobite.member.dao.entry.PersonFunction;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2008-用户功能设置")
@RestService
public class PersonFunctionAction {
    PersonFunctionDao personFunctionDao = new PersonFunctionDao();

    @ApiDoc(title = "查询用户功能列表", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/personFunction/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        PersonFunction personFunction = new PersonFunction();
        personFunction.setPersonId(Long.parseLong(form.get("personId")));
        return new Result( personFunctionDao.list(personFunction, PersonFunction.class));
    }

    @ApiDoc(title = "查询用户功能列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/personFunction/all")
    @Proxy(target = AuthProxy.class)
    public Result findAll(RestForm form) {
        PersonFunction personFunction = new PersonFunction();
        personFunction.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        return new Result( personFunctionDao.list(personFunction, PersonFunction.class));
    }

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/personFunction/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form) {
        PersonFunction personFunction = new PersonFunction();
        personFunction.setPersonId(Long.parseLong(form.get("personId")));
        personFunction.setFunctionId(Long.parseLong(form.get("functionId")));
        personFunctionDao.add(personFunction);
        return new Result(200);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "functionId", type = "string", desc = "功能ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/personFunction/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        PersonFunction personFunction = new PersonFunction();
        personFunction.setPersonId(Long.parseLong(form.get("personId")));
        personFunction.setFunctionId(Long.parseLong(form.get("functionId")));
        personFunctionDao.remove(personFunction);
        return new Result(200);
    }
}
