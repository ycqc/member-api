package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;


import ltd.trilobite.member.dao.MemberOrderDao;
import ltd.trilobite.member.dao.PersonDao;
import ltd.trilobite.member.dao.entry.MemberOrder;
import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.member.service.OrderService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3002-会员权益订单")
@RestService
public class MemberOrderAction {

    MemberOrderDao memberOrderDao = new MemberOrderDao();
    PersonDao personDao = new PersonDao();
    OrderService orderService = new OrderService();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "memberProductId", type = "header", desc = "产品编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/member/memberOrder/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param MemberOrder memberOrder) {
        Person person = new Person();
        person.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        Person personResult = personDao.findOne(person, Person.class);
        if (personResult.getMemberType() != null) {
            memberOrder.setPersonId(Long.parseLong(form.getHeader().get("personId")));
            memberOrder.setOrderState(1);
            memberOrder.setCreateTime(null);
        } else {
            return new Result(-1);
        }
        memberOrderDao.add(memberOrder);
        return new Result(200);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberOrder/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param MemberOrder memberOrder) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(memberOrder.getOrderId());
        memberOrderDao.del(order);
        return new Result(true);
    }

    @ApiDoc(title = "修改凭证", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberOrder/updateCredential")
    @Proxy(target = AuthProxy.class)
    public Result updateCredential(RestForm form, @Param MemberOrder memberOrder) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(memberOrder.getOrderId());
        order.setPayCredential(memberOrder.getPayCredential());
        memberOrderDao.update(order);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为待处理", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberOrder/tagState-1")
    @Proxy(target = AuthProxy.class)
    public Result tagState(RestForm form, @Param MemberOrder memberOrder) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(memberOrder.getOrderId());
        order.setOrderState(1);
        memberOrderDao.update(order);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为处理中", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberOrder/tagState-2")
    @Proxy(target = AuthProxy.class)
    public Result tagState2(RestForm form, @Param MemberOrder memberOrder) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(memberOrder.getOrderId());
        order.setOrderState(2);
        memberOrderDao.update(order);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为完成", param = {
            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberOrder/tagState-3")
    @Proxy(target = AuthProxy.class)
    public Result tagState3(RestForm form, @Param MemberOrder memberOrder) {
        MemberOrder order = new MemberOrder();
        order.setOrderId(memberOrder.getOrderId());
        order.setOrderState(3);
        memberOrderDao.update(order);
        new Thread(() -> {

            orderService.timelyRebate(memberOrderDao.findOne(order, MemberOrder.class));
        }).start();

        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberOrderId", type = "string", desc = "编号"),
                    @ApiDesc(name = "name", type = "string", desc = "权益"),
                    @ApiDesc(name = "price", type = "header", desc = "费用"),
            }
    )
    @Router(path = "/member/memberOrder/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param MemberOrder memberOrder) {
        return new Result(memberOrderDao.list(memberOrder, MemberOrder.class));
    }

    @ApiDoc(title = "查询详细信息", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberOrderId", type = "string", desc = "编号"),
                    @ApiDesc(name = "name", type = "string", desc = "权益"),
                    @ApiDesc(name = "price", type = "header", desc = "费用"),
            }
    )
    @Router(path = "/member/memberOrder/findOne", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param MemberOrder memberOrder) {
        return new Result(memberOrderDao.findInfo(memberOrder));
    }

    //"mo.order_id","mo.pay_credential","mo.create_time","mo.order_state","mp.name","mp.price"
    @ApiDoc(title = "我的订单－分页查询", param = {
            @ApiDesc(name = "orderState", type = "string", desc = "订单状态"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "编号"),
                    @ApiDesc(name = "payCredential", type = "string", desc = "支付凭证"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
                    @ApiDesc(name = "orderState", type = "string", desc = "订单状态"),
                    @ApiDesc(name = "name", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),

            }
    )
    @Router(path = "/member/memberOrder/myNaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result myNaviList(RestForm form, @Param MemberOrder memberOrder) {
        Long PersonId = Long.parseLong(form.getHeader().get("personId"));
        return memberOrderDao.findmyNaviList(PersonId, memberOrder, form);
    }


    @ApiDoc(title = "分页查询", param = {
            @ApiDesc(name = "orderState", type = "string", desc = "订单状态"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "orderId", type = "string", desc = "编号"),
                    @ApiDesc(name = "payCredential", type = "string", desc = "支付凭证"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
                    @ApiDesc(name = "orderState", type = "string", desc = "订单状态"),
                    @ApiDesc(name = "name", type = "string", desc = "产品名称"),
                    @ApiDesc(name = "price", type = "string", desc = "价格"),

            }
    )
    @Router(path = "/member/memberOrder/NaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result naviList(RestForm form, @Param Person person, @Param MemberOrder memberOrder) {
        return memberOrderDao.findNaviList(memberOrder, person, form);
    }


}
