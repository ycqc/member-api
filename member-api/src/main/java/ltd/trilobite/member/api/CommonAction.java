package ltd.trilobite.member.api;

import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.service.CommonService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1-通用接口")
@RestService
public class CommonAction {
    CommonService commonService = new CommonService();

    @ApiDoc(title = "客户端编号", result = {@ApiDesc(name = "data", type = "string", desc = "客户端编号")})
    @Router(path = "/ClientId")
    public Result getClientId() {
        return new Result(commonService.getClientId());
    }
}
