package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.PerformanceDifferConfigDao;
import ltd.trilobite.member.dao.entry.PerformanceDifferConfig;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2004-绩差设置")
@RestService
public class PerformanceDifferConfigAction {

    PerformanceDifferConfigDao performanceDifferConfigDao = new PerformanceDifferConfigDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "levelNum", type = "string", desc = "级别"),
            @ApiDesc(name = "achievement", type = "string", desc = "市场业绩"),
            @ApiDesc(name = "proportion", type = "string", desc = "分配比例"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/performanceDifferConfig/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param PerformanceDifferConfig performanceDifferConfig) {
        performanceDifferConfigDao.add(performanceDifferConfig);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "levelNum", type = "string", desc = "级别"),
            @ApiDesc(name = "achievement", type = "string", desc = "市场业绩"),
            @ApiDesc(name = "proportion", type = "string", desc = "分配比例"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/performanceDifferConfig/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param PerformanceDifferConfig performanceDifferConfig) {
        performanceDifferConfigDao.update(performanceDifferConfig);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "levelNum", type = "string", desc = "级别"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/performanceDifferConfig/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param PerformanceDifferConfig performanceDifferConfig) {
        performanceDifferConfigDao.del(performanceDifferConfig);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "levelNum", type = "string", desc = "级别"),
                    @ApiDesc(name = "achievement", type = "string", desc = "市场业绩"),
                    @ApiDesc(name = "proportion", type = "string", desc = "分配比例"),
            }
    )
    @Router(path = "/sysset/performanceDifferConfig/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param PerformanceDifferConfig performanceDifferConfig) {
        return new Result(performanceDifferConfigDao.list(performanceDifferConfig, PerformanceDifferConfig.class));
    }
}
