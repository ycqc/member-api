package ltd.trilobite.member.api;

import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.service.PersonService;
import ltd.trilobite.member.service.SmsService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1006-验证码接口")
@RestService
public class SmsAction {
    SmsService smsService=new SmsService();

    @ApiDoc(title = "发送验证码", param = {
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
    }
            , result = {@ApiDesc(name = "data", type = "string", desc = "-1 发送失败，-2 已成功发送消息，请５分钟后再次发送 200 发送成功")})
    @Router(path = "/sendSms")
    public Result sendSms(RestForm form) {
        int code=smsService.sendVcode(form.get("phoneNum"));
        return new Result(code,"");
    }


    @ApiDoc(title = "短信验证", param = {
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
            @ApiDesc(name = "vcode", type = "string", desc = "验证码"),
    }
            , result = {@ApiDesc(name = "data", type = "string", desc = "-1 验证失败， 200 验证通过")})
    @Router(path = "/smsValiCode")
    public Result valiCode(RestForm form) {
        int code=smsService.valiCode(form.get("phoneNum"),form.get("vcode"));
        return new Result(code,"");
    }


}
