package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.CartInfoDao;
import ltd.trilobite.member.dao.entry.CartInfo;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2005-车辆信息")
@RestService
public class CartInfoAction {


    CartInfoDao cartInfoDao = new CartInfoDao();

    @ApiDoc(title = "新增", param = {


            @ApiDesc(name = "carNum", type = "string", desc = "车牌号"),
            @ApiDesc(name = "title", type = "string", desc = "说明"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/cartInfo/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param CartInfo cartInfo) {
        cartInfoDao.add(cartInfo);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "cartInfoId", type = "string", desc = "编号"),
            @ApiDesc(name = "carNum", type = "string", desc = "车牌号"),
            @ApiDesc(name = "title", type = "string", desc = "说明"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/cartInfo/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param CartInfo cartInfo) {
        cartInfoDao.update(cartInfo);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "cartInfoId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/cartInfo/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param CartInfo cartInfo) {
        cartInfoDao.del(cartInfo);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "cartInfoId", type = "string", desc = "编号"),
                    @ApiDesc(name = "carNum", type = "string", desc = "车牌号"),
                    @ApiDesc(name = "title", type = "string", desc = "说明"),
            }
    )
    @Router(path = "/sysset/cartInfo/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param CartInfo cartInfo) {
        return new Result(cartInfoDao.list(cartInfo, CartInfo.class));
    }
}
