package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.FunctionDao;
import ltd.trilobite.member.dao.RecommendRewardConfigDao;
import ltd.trilobite.member.dao.entry.Function;
import ltd.trilobite.member.dao.entry.RecommendRewardConfig;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2010-推荐奖励设置")
@RestService
public class RecommendRewardConfigAction {
    RecommendRewardConfigDao recommendRewardConfigDao = new RecommendRewardConfigDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "recommendRewardConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "layer", type = "string", desc = "层"),
            @ApiDesc(name = "proportion", type = "string", desc = "分配比率"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendRewardConfig/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param RecommendRewardConfig recommendRewardConfig) {
        RecommendRewardConfig queryConfig = recommendRewardConfigDao.findById(recommendRewardConfig.getRecommendRewardConfigId());
        if(queryConfig != null){
            return new Result(-1);
        }
        recommendRewardConfigDao.add(recommendRewardConfig);
        return new Result(200);
    }

    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "recommendRewardConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "layer", type = "string", desc = "层"),
            @ApiDesc(name = "proportion", type = "string", desc = "分配比率"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendRewardConfig/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param RecommendRewardConfig recommendRewardConfig) {
        recommendRewardConfigDao.update(recommendRewardConfig);
        return new Result(200);
    }


    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "recommendRewardConfigId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "成功-200 -1-失败"),
            }

    )
    @Router(path = "/sysset/recommendRewardConfig/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form) {
        RecommendRewardConfig config = new RecommendRewardConfig();
        config.setRecommendRewardConfigId(Long.parseLong(form.get("functionId")));
        recommendRewardConfigDao.del(config);
        return new Result(200);
    }

    @ApiDoc(title = "列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "recommendRewardConfigId", type = "string", desc = "编号"),
                    @ApiDesc(name = "layer", type = "string", desc = "层"),
                    @ApiDesc(name = "proportion", type = "string", desc = "分配比率"),
            }
    )
    @Router(path = "/sysset/recommendRewardConfig/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return new Result(recommendRewardConfigDao.list(new RecommendRewardConfig(), RecommendRewardConfig.class));
    }
}
