package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.entry.PersonCertification;
import ltd.trilobite.member.service.PersonCertificationService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4001-实名认证")
@RestService
public class PersonCertificationAction {
    PersonCertificationService personCertificationService = new PersonCertificationService();

    @ApiDoc(title = "保存实名认证信息", param = {
            @ApiDesc(name = "realName", type = "string", desc = "姓名"),
            @ApiDesc(name = "cardFront", type = "string", desc = "身份证正面"),
            @ApiDesc(name = "cardBg", type = "string", desc = "身份证背面"),
            @ApiDesc(name = "handImg", type = "string", desc = "手持身份证"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/member/certification/Save", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result save(RestForm form) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        personCertification.setRealName(form.get("realName"));
        personCertification.setCardFront(form.get("cardFront"));
        personCertification.setCardBg(form.get("cardBg"));
        personCertification.setHandImg(form.get("handImg"));
        return personCertificationService.save(personCertification);
    }

    @ApiDoc(title = "设置审核中", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/member/certification/AuditStateToChecking", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result setAuditStateToChecking(RestForm form) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setPersonId(Long.parseLong(form.get("personId")));
        personCertification.setAuditPerson( Long.parseLong(form.getHeader().get("personId")));
        return personCertificationService.setAuditStateToChecking(personCertification);
    }

    @ApiDoc(title = "设置审核完成", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/member/certification/AuditStateToComplete", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result setAuditStateToComplete(RestForm form) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setPersonId(Long.parseLong(form.get("personId")));
        personCertification.setAuditPerson( Long.parseLong(form.getHeader().get("personId")));
        return personCertificationService.setAuditStateToComplete(personCertification);
    }

    @ApiDoc(title = "设置审核通过", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "cardNo", type = "string", desc = "身份证号"),
            @ApiDesc(name = "province", type = "string", desc = "省份编码"),
            @ApiDesc(name = "city", type = "string", desc = "城市编码"),
            @ApiDesc(name = "area", type = "string", desc = "地区编码"),
            @ApiDesc(name = "street", type = "string", desc = "街道代码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/member/certification/AuditOk", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result auditOk(RestForm form) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setCardNo(form.get("cardNo"));
        personCertification.setProvince(form.get("province"));
        personCertification.setCity(form.get("city"));
        personCertification.setArea(form.get("area"));
        personCertification.setStreet(form.get("street"));
        personCertification.setPersonId(Long.parseLong(form.get("personId")));
        personCertification.setAuditPerson( Long.parseLong(form.getHeader().get("personId")));
        return personCertificationService.auditOk(personCertification);
    }

    @ApiDoc(title = "审核未通过", param = {
            @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
            @ApiDesc(name = "auditMessage", type = "string", desc = "审核消息"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
                    @ApiDesc(name = "data", type = "string", desc = "消息 1-成功 -1-失败")
            }

    )
    @Router(path = "/member/certification/AuditFail", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result auditFail(RestForm form) {
        PersonCertification personCertification = new PersonCertification();
        personCertification.setAuditMessage(form.get("auditMessage"));
        personCertification.setPersonId(Long.parseLong(form.get("personId")));
        personCertification.setAuditPerson(Long.parseLong(form.getHeader().get("personId")));
        return personCertificationService.auditFail(personCertification);
    }

    @ApiDoc(title = "审核信息", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "realName", type = "string", desc = "姓名"),
                    @ApiDesc(name = "cardNo", type = "string", desc = "身份证号"),
                    @ApiDesc(name = "cardFront", type = "string", desc = "身份证正面"),
                    @ApiDesc(name = "cardBg", type = "string", desc = "身份证背面"),
                    @ApiDesc(name = "handImg", type = "string", desc = "手持省份证"),
                    @ApiDesc(name = "province", type = "string", desc = "省份编码"),
                    @ApiDesc(name = "city", type = "string", desc = "城市编码"),
                    @ApiDesc(name = "area", type = "string", desc = "地区编码"),
                    @ApiDesc(name = "street", type = "string", desc = "街道编码"),
                    @ApiDesc(name = "isAudit", type = "string", desc = "是否通过 1-通过 2-未通过"),
                    @ApiDesc(name = "auditState", type = "string", desc = "审核状态 1-待审核 2-审核中 3-已审核"),
                    @ApiDesc(name = "auditMessage", type = "string", desc = "失败原因"),
                    @ApiDesc(name = "updateTime", type = "string", desc = "更新时间"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }

    )
    @Router(path = "/member/certification/Info", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findByToken(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(personCertificationService.findByPersonId(personId));
    }

    @ApiDoc(title = "分页列表", param = {
            @ApiDesc(name = "name", type = "string", desc = "用户名"),
            @ApiDesc(name = "realName", type = "string", desc = "姓名"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
            @ApiDesc(name = "cardNo", type = "string", desc = "身份证号码"),
            @ApiDesc(name = "isAudit", type = "string", desc = "是否通过 1-通过 2-未通过"),
            @ApiDesc(name = "auditState", type = "string", desc = "审核状态 1-待审核 2-审核中 3-已审核"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),

    },
            result = {
                    @ApiDesc(name = "personId", type = "string", desc = "用户ID"),
                    @ApiDesc(name = "name", type = "string", desc = "用户名"),
                    @ApiDesc(name = "realName", type = "string", desc = "姓名"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
                    @ApiDesc(name = "cardNo", type = "string", desc = "身份证号"),
                    @ApiDesc(name = "cardFront", type = "string", desc = "身份证正面"),
                    @ApiDesc(name = "cardBg", type = "string", desc = "身份证背面"),
                    @ApiDesc(name = "handImg", type = "string", desc = "手持省份证"),
                    @ApiDesc(name = "provinceName", type = "string", desc = "省份"),
                    @ApiDesc(name = "cityName", type = "string", desc = "城市"),
                    @ApiDesc(name = "areaName", type = "string", desc = "地区"),
                    @ApiDesc(name = "street", type = "string", desc = "街道"),
                    @ApiDesc(name = "province", type = "string", desc = "省份编码"),
                    @ApiDesc(name = "city", type = "string", desc = "城市编码"),
                    @ApiDesc(name = "area", type = "string", desc = "地区编码"),
                    @ApiDesc(name = "street", type = "string", desc = "街道编码"),
                    @ApiDesc(name = "isAudit", type = "string", desc = "是否通过 1-通过 2-未通过"),
                    @ApiDesc(name = "auditState", type = "string", desc = "审核状态 1-待审核 2-审核中 3-已审核"),
                    @ApiDesc(name = "auditMessage", type = "string", desc = "失败原因"),
                    @ApiDesc(name = "auditName", type = "string", desc = "审核人"),
                    @ApiDesc(name = "updateTime", type = "string", desc = "更新时间"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }
    )
    @Router(path = "/member/certification/NaviList", method = Router.Action.Post)
    @Proxy(target = AuthProxy.class)
    public Result findNaviList(RestForm form) {
        return personCertificationService.findNaviList(form);
    }
}
