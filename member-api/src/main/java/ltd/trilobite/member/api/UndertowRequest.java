package ltd.trilobite.member.api;

import io.undertow.server.HttpServerExchange;
import org.apache.commons.fileupload.RequestContext;

import java.io.IOException;
import java.io.InputStream;

public class UndertowRequest implements RequestContext {
    HttpServerExchange request;

    public UndertowRequest(HttpServerExchange httpServerExchange) {
        request = httpServerExchange;
    }

    @Override
    public String getCharacterEncoding() {
        return request.getRequestCharset();
    }

    @Override
    public String getContentType() {
        return request.getRequestHeaders().getFirst("Content-Type");
    }

    @Override
    public int getContentLength() {
        return Integer.valueOf(request.getRequestHeaders().getFirst("Content-Length").toString());
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return request.getInputStream();
    }
}