package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.BaseDataDao;
import ltd.trilobite.member.dao.GiveGoodsConfigDao;
import ltd.trilobite.member.dao.MemberProductDao;
import ltd.trilobite.member.dao.entry.BaseData;
import ltd.trilobite.member.dao.entry.GiveGoodsConfig;
import ltd.trilobite.member.dao.entry.MemberProduct;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.HashMap;
import java.util.Map;

@ApiDesc(desc = "3001-会员权益产品")
@RestService
public class MemberProductAction {


    MemberProductDao memberProductDao = new MemberProductDao();
    GiveGoodsConfigDao giveGoodsConfigDao = new GiveGoodsConfigDao();
    BaseDataDao baseDataDao = new BaseDataDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
            @ApiDesc(name = "name", type = "string", desc = "权益"),
            @ApiDesc(name = "price", type = "header", desc = "费用"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/member/memberProduct/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param MemberProduct memberProduct) {
        memberProductDao.add(memberProduct);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
            @ApiDesc(name = "name", type = "string", desc = "权益"),
            @ApiDesc(name = "price", type = "header", desc = "费用"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberProduct/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param MemberProduct memberProduct) {
        memberProductDao.update(memberProduct);

        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/memberProduct/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param MemberProduct memberProduct) {
        memberProductDao.del(memberProduct);
        return new Result(true);
    }


    @ApiDoc(title = "查询详细信息", param = {
            @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "title.name", type = "string", desc = "标题"),
                    @ApiDesc(name = "title.price", type = "string", desc = "费用"),
                    @ApiDesc(name = "givegood.goodName", type = "string", desc = "物品名称"),
                    @ApiDesc(name = "givegood.num", type = "string", desc = "数量"),
                    @ApiDesc(name = "givegood.unit", type = "string", desc = "单位"),
                    @ApiDesc(name = "options.code", type = "string", desc = "代码"),
                    @ApiDesc(name = "options.label", type = "string", desc = "奖励项"),
            }
    )
    @Router(path = "/member/memberProduct/findProductInfo")
    @Proxy(target = AuthProxy.class)
    public Result findProductInfo(RestForm form, @Param MemberProduct memberProduct) {
        MemberProduct parma = new MemberProduct();
        parma.setMemberProductId(memberProduct.getMemberProductId());
        Object title = memberProductDao.findOne(parma, MemberProduct.class);
        GiveGoodsConfig giveGoodsConfig = new GiveGoodsConfig();
        giveGoodsConfig.setProductId(memberProduct.getMemberProductId());
        Object givegood = giveGoodsConfigDao.list(giveGoodsConfig, GiveGoodsConfig.class);
        BaseData baseData = new BaseData();
        baseData.setTypeId(2);
        Object options = baseDataDao.list(baseData, BaseData.class);
        Map re = new HashMap<>();
        re.put("title", title);
        re.put("options", options);
        re.put("givegood", givegood);
        return new Result(re);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
                    @ApiDesc(name = "name", type = "string", desc = "权益"),
                    @ApiDesc(name = "price", type = "header", desc = "费用"),
            }
    )
    @Router(path = "/member/memberProduct/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param MemberProduct memberProduct) {
        return new Result(memberProductDao.list(memberProduct, MemberProduct.class));
    }


    @ApiDoc(title = "我订的产品统计", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberProductId", type = "string", desc = "编号"),
                    @ApiDesc(name = "name", type = "string", desc = "权益"),
                    @ApiDesc(name = "price", type = "header", desc = "费用"),
            }
    )
    @Router(path = "/member/memberProduct/myProductCount", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result myProductCount(RestForm form, @Param MemberProduct memberProduct) {

        return new Result(memberProductDao.myProductCount(memberProduct, Long.parseLong(form.getHeader().get("personId"))));
    }


}
