package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.BaseDataDao;
import ltd.trilobite.member.dao.entry.BaseData;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2003-基础数据")
@RestService
public class BaseDataAction {


    BaseDataDao baseDataDao = new BaseDataDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "code", type = "string", desc = "代码"),
            @ApiDesc(name = "label", type = "string", desc = "显示值"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/baseData/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param BaseData baseData) {
        baseDataDao.add(baseData);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "baseId", type = "string", desc = "编号"),
            @ApiDesc(name = "code", type = "string", desc = "代码"),
            @ApiDesc(name = "label", type = "string", desc = "显示值"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/baseData/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param BaseData baseData) {
        baseDataDao.update(baseData);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "baseDataId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/baseData/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param BaseData baseData) {
        baseDataDao.del(baseData);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "baseId", type = "string", desc = "编号"),
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "label", type = "string", desc = "显示值"),
                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )
    @Router(path = "/sysset/baseData/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param BaseData baseData) {
        return new Result(baseDataDao.list(baseData, BaseData.class));
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "baseId", type = "string", desc = "编号"),
                    @ApiDesc(name = "code", type = "string", desc = "代码"),
                    @ApiDesc(name = "label", type = "string", desc = "显示值"),
                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )
    @Router(path = "/sysset/baseData/findSortList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findSortList(RestForm form, @Param BaseData baseData) {
        return new Result(baseDataDao.sortList(baseData));
    }
}
