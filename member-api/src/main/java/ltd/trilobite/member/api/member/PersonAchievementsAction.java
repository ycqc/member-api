package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.PersonAchievementsDao;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3006-我的业绩")
@RestService
public class PersonAchievementsAction {
    PersonAchievementsDao personAchievementsDao = new PersonAchievementsDao();

    @ApiDoc(title = "我的业绩统计", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "line", type = "string", desc = "直推"),
                    @ApiDesc(name = "all", type = "string", desc = "所有"),
                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )

    @Router(path = "/member/personAchievements/myAchievements")
    @Proxy(target = AuthProxy.class)
    public Result myAchievements(RestForm form) {
        return new Result(personAchievementsDao.myAchievements(Long.parseLong(form.getHeader().get("personId"))));
    }

    @ApiDoc(title = "我的业绩日增长量", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "count", type = "string", desc = "份数"),
                    @ApiDesc(name = "createTime", type = "string", desc = "日期"),
                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )

    @Router(path = "/member/personAchievements/myDayList")
    @Proxy(target = AuthProxy.class)
    public Result myDayList(RestForm form) {
        return personAchievementsDao.myDayList(Long.parseLong(form.getHeader().get("personId")), form);
    }
}
