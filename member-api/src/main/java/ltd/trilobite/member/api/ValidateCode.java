package ltd.trilobite.member.api;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;
import ltd.trilobite.common.service.CommonService;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.util.Random;

public class ValidateCode implements HttpHandler {

    char[] code = {'1', '2', '3', '4', '5', '6', '7', '8',
            '9', '0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'
            , 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    CommonService commonService = new CommonService();

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        httpServerExchange.startBlocking();
        HeaderMap header = httpServerExchange.getResponseHeaders();
        header.add(new HttpString("Pragma"), "no-cache");
        header.add(new HttpString("Cache-Control"), "no-cache");
        header.add(new HttpString("Expires"), -1);
        header.add(new HttpString("Content-Type"), "image/png");
        header.add(new HttpString("Access-Control-Allow-Origin"), "*");

        OutputStream out = httpServerExchange.getOutputStream();
        BufferedImage buffImg = new BufferedImage(110, 40, BufferedImage.TYPE_INT_RGB);
        Graphics gd = buffImg.getGraphics();
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, 110, 40);
        Font font = new Font("Fixedsys", Font.BOLD, 36);
        gd.setFont(font);
        gd.setColor(Color.blue);
        Random random = new Random();
        char[] chr = new char[4];
        chr[0] = code[random.nextInt(36)];
        chr[1] = code[random.nextInt(36)];
        chr[2] = code[random.nextInt(36)];
        chr[3] = code[random.nextInt(36)];
        String vcode = new String(chr);
        commonService.VCode(vcode, httpServerExchange.getQueryParameters().get("token").getFirst());
        gd.drawString(vcode + "", 2, 36 - 2);

        ImageIO.write(buffImg, "png", out);

    }
}
