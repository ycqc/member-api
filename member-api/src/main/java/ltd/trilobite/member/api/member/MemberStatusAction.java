package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;

import ltd.trilobite.member.dao.MemberStatusDao;
import ltd.trilobite.member.dao.PersonDao;
import ltd.trilobite.member.dao.entry.MemberStatus;
import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1003-会员结构")
@RestService
public class MemberStatusAction {


    MemberStatusDao memberStatusDao = new MemberStatusDao();
    PersonDao personDao = new PersonDao();

    @ApiDoc(title = "新增", param = {


            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/member/memberStatus/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param Person person) {
        Person param = new Person();
        param.setPhoneNum(person.getPhoneNum());
        Person personInfo = personDao.findOne(param, Person.class);
        if (personInfo == null) {
            return new Result(-2);//不存在的账号
        }
        MemberStatus memberStatus = new MemberStatus();
        memberStatus.setMemberId(Long.parseLong(form.getHeader().get("personId")));
        MemberStatus result = memberStatusDao.findOne(memberStatus, MemberStatus.class);
        Person selfperson = new Person();
        selfperson.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        if (personDao.findOne(selfperson, Person.class).getPhoneNum().equals(person.getPhoneNum())) {
            return new Result(-3);//不能将自己的账号做为推荐人
        }

        if (result == null) {
            memberStatus.setParentId(personInfo.getPersonId());
            memberStatusDao.add(memberStatus);
        } else {
            if (result.getParentId() != null) {
                return new Result(-1);//已经存在推荐人
            }
            result.setParentId(personInfo.getPersonId());
            memberStatusDao.update(result);

        }
        memberStatus.setMemberId(null);
        memberStatus.setParentId(personInfo.getPersonId());
        Long count = memberStatusDao.findFunc(memberStatus, Long.class, " count(1) ");
        personInfo.setTreamNum(count.intValue());
        personDao.update(personInfo);

        return new Result(200);
    }

    @ApiDoc(title = "查询推荐人", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
            }
    )
    @Router(path = "/member/memberStatus/findParentPerson", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findParentPerson(RestForm form) {

        MemberStatus memberStatus = new MemberStatus();
        memberStatus.setMemberId(Long.parseLong(form.getHeader().get("personId")));
        MemberStatus result = memberStatusDao.findOne(memberStatus, MemberStatus.class);
        if (result != null) {
            Person param = new Person();
            param.setPersonId(result.getParentId());
            Person personInfo = personDao.findOne(param, Person.class);
            personInfo.setPersonId(null);
            return new Result(personInfo);
        }
        return new Result(200);

    }

    @ApiDoc(title = "查询朋友圈", param = {
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
                    @ApiDesc(name = "treamNum", type = "string", desc = "团队数量"),
                    @ApiDesc(name = "treamPark", type = "string", desc = "团队份额"),
                    @ApiDesc(name = "memberType", type = "string", desc = "会员类型"),
                    @ApiDesc(name = "headImageUrl", type = "string", desc = "头像"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }
    )
    @Router(path = "/member/memberStatus/findSubPerson", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findSubPerson(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return memberStatusDao.findSubMember(personId, form);
    }

    @ApiDoc(title = "查询推荐人", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
                    @ApiDesc(name = "treamNum", type = "string", desc = "团队数量"),
                    @ApiDesc(name = "treamPark", type = "string", desc = "团队份额"),
                    @ApiDesc(name = "memberType", type = "string", desc = "会员类型"),
                    @ApiDesc(name = "headImageUrl", type = "string", desc = "头像"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }
    )
    @Router(path = "/member/memberStatus/findRecommendPerson", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findRecommendPerson(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(memberStatusDao.findRecommendPerson(personId));
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "memberStatusId", type = "string", desc = "编号"),
                    @ApiDesc(name = "carNum", type = "string", desc = "车牌号"),
                    @ApiDesc(name = "title", type = "string", desc = "说明"),
            }
    )
    @Router(path = "/member/memberStatus/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param MemberStatus memberStatus) {
        return new Result(memberStatusDao.list(memberStatus, MemberStatus.class));
    }
}
