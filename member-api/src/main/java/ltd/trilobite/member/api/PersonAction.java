package ltd.trilobite.member.api;

import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.service.PersonService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1001-人员信息接口")
@RestService
public class PersonAction {
    PersonService personService = new PersonService();

    @ApiDoc(title = "会员登录", param = {
            @ApiDesc(name = "username", type = "string", desc = "用户名"),
            @ApiDesc(name = "password", type = "string", desc = "密码"),
            @ApiDesc(name = "vcode", type = "string", desc = "验证码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),

    }
            , result = {@ApiDesc(name = "data", type = "string", desc = "-1 验证码失败，-2 用户或密码输入错误 -3 验证码超时")})
    @Router(path = "/Login")
    public Result Login(RestForm form) {
        return personService.memberLogin(form.get("username"), form.get("password"), form.get("vcode"), form.getHeader().get("token"));
    }


}
