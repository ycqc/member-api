package ltd.trilobite.member.api;


import com.alibaba.fastjson.JSON;
import io.undertow.io.Sender;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.SortUUID;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class HttpFileUpload implements HttpHandler {
    // ByteRangeHandler byteRangeHandler=null;
    Map<String, String> suportType = new HashMap<>();

    public HttpFileUpload() {
        suportType.put("image/jpeg", "jpg");
        suportType.put("image/png", "png");
        suportType.put("video/mpeg4", "mp4");
    }

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        System.out.println("处理文件内容");
        httpServerExchange.startBlocking();
        DiskFileItemFactory factory = new DiskFileItemFactory();

        File uploaderdir = new File("/opt/upload" + new SimpleDateFormat("/YYYY/MM/dd/").format(new Date()));
        factory.setRepository(new File("/opt/upload"));
        FileUpload fileUpload = new FileUpload(factory);
        fileUpload.setFileSizeMax(1024*1024*10);
        fileUpload.setSizeMax(1024*1024*10);
        fileUpload.setHeaderEncoding("UTF-8");
        UndertowRequest request = new UndertowRequest(httpServerExchange);

        List<FileItem> list = fileUpload.parseRequest(request);
        Iterator it = list.iterator();

        if (!uploaderdir.exists()) {
            uploaderdir.mkdirs();
        }


        String hostname = "";
        String fileName = "";
        String path = "";
        String rpath = "";
        while (it.hasNext()) {
            try {
                FileItem f = (FileItem) it.next();

                if (f.isFormField()) {

                } else {
                    System.out.println(f.getHeaders());
                    System.out.println(f.getName());
                    System.out.println(f.getContentType());
                    System.out.println(f.getSize());
                    //request.getInputStream()
                    // System.out.println(f.getString());
                    //f.write();
                    fileName = SortUUID.uuid() + "." + suportType.get(f.getContentType());
                    path = uploaderdir.getAbsolutePath() + "/" + fileName;
                    rpath = path.replaceAll("/opt/upload", "");
                    System.out.println(path);
                    System.out.println(rpath);
                    f.write(new File(path));
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Sender out = httpServerExchange.getResponseSender();
        Result result = new Result(200);
        result.setData(rpath);
        out.send(JSON.toJSONString(result));

    }
}