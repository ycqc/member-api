package ltd.trilobite.member.api;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.SortUUID;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@ApiDesc(desc="1005-上传html")
@RestService
public class HtmlUploadAction {
    private static final String SAVE_PATH = "/opt/upload";

    @ApiDoc(title = "上传html", param = {
            @ApiDesc(name = "content", type = "string", desc = "html内容"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/htmlUpload")
    @Proxy(target = AuthProxy.class)
    public Result upload(RestForm form) {
        String path = "/html/" + new SimpleDateFormat("yyyy/MM/dd").format(new Date()) + "/" + SortUUID.uuid()+".html";
        try {
            FileUtils.writeStringToFile(new File(SAVE_PATH + path), form.get("content"), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(-1, -1, true);
        }
        return new Result(path);
    }
}
