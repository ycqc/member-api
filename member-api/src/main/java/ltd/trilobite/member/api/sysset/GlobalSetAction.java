package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.GlobalSetDao;
import ltd.trilobite.member.dao.entry.GlobalSet;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2002-全局设置")
@RestService
public class GlobalSetAction {


    GlobalSetDao globalSetDao = new GlobalSetDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "globalSetId", type = "string", desc = "编号"),
            @ApiDesc(name = "constName", type = "string", desc = "变量名称"),
            @ApiDesc(name = "constVal", type = "string", desc = "变量值"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/globalSet/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param GlobalSet globalSet) {
        globalSetDao.add(globalSet);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "globalSetId", type = "string", desc = "编号"),
            @ApiDesc(name = "constName", type = "string", desc = "变量名称"),
            @ApiDesc(name = "constVal", type = "string", desc = "变量值"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/globalSet/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param GlobalSet globalSet) {
        globalSetDao.update(globalSet);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "globalSetId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/globalSet/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param GlobalSet globalSet) {
        globalSetDao.del(globalSet);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "globalSetId", type = "string", desc = "编号"),
                    @ApiDesc(name = "constName", type = "string", desc = "变量名称"),
                    @ApiDesc(name = "constVal", type = "string", desc = "变量值"),
            }
    )
    @Router(path = "/sysset/globalSet/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param GlobalSet globalSet) {
        return new Result(globalSetDao.list(globalSet, GlobalSet.class));
    }
}
