package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.AchievementsSettlementDao;
import ltd.trilobite.member.dao.entry.AchievementsSettlement;
import ltd.trilobite.member.service.OrderService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3004-绩差结算")
@RestService
public class AchievementsSettlementAction {
    AchievementsSettlementDao ahievementsSettlementDao = new AchievementsSettlementDao();
    OrderService orderService = new OrderService();

    @ApiDoc(title = "新增", param = {


            @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
            @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
            @ApiDesc(name = "description", type = "string", desc = "说明"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/member/achievementsSettlement/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        achievementsSettlement.setState(1);
        ahievementsSettlementDao.add(achievementsSettlement);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
            @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
            @ApiDesc(name = "description", type = "string", desc = "说明"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/achievementsSettlement/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        ahievementsSettlementDao.update(achievementsSettlement);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "achievementsSettlementId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/achievementsSettlement/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        ahievementsSettlementDao.del(achievementsSettlement);
        return new Result(true);
    }

    @ApiDoc(title = "分页查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "achievementsSettlementId", type = "string", desc = "编号"),
                    @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
                    @ApiDesc(name = "description", type = "string", desc = "说明"),
            }
    )
    @Router(path = "/member/achievementsSettlement/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        return ahievementsSettlementDao.navilist(form, achievementsSettlement);
    }


    @ApiDoc(title = "标记状态为待处理", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/achievementsSettlement/tagState-1")
    @Proxy(target = AuthProxy.class)
    public Result tagState(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        AchievementsSettlement param = new AchievementsSettlement();
        param.setAchievementsSettlementId(achievementsSettlement.getAchievementsSettlementId());
        param.setState(1);
        ahievementsSettlementDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为处理中", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/achievementsSettlement/tagState-2")
    @Proxy(target = AuthProxy.class)
    public Result tagState2(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        AchievementsSettlement param = new AchievementsSettlement();
        param.setAchievementsSettlementId(achievementsSettlement.getAchievementsSettlementId());
        param.setState(2);
        ahievementsSettlementDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为完成", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/achievementsSettlement/tagState-3")
    @Proxy(target = AuthProxy.class)
    public Result tagState3(RestForm form, @Param AchievementsSettlement achievementsSettlement) {
        AchievementsSettlement param = new AchievementsSettlement();
        param.setAchievementsSettlementId(achievementsSettlement.getAchievementsSettlementId());

        new Thread(() -> {
            orderService.performanceDifference(ahievementsSettlementDao.findOne(param, AchievementsSettlement.class));
            param.setState(3);
            ahievementsSettlementDao.update(param);
        }).start();

        return new Result(true);
    }
}
