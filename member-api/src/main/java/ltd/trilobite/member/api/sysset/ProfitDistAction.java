package ltd.trilobite.member.api.sysset;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.ProfitDistDao;
import ltd.trilobite.member.dao.entry.ProfitDist;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2001-分润设置")
@RestService
public class ProfitDistAction {

    ProfitDistDao profitDistDao = new ProfitDistDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
            @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
            @ApiDesc(name = "total", type = "string", desc = "总收益"),
            @ApiDesc(name = "mStartTime", type = "string", desc = "经营权范围开始"),
            @ApiDesc(name = "mEndTime", type = "string", desc = "经营券范围结束"),
            @ApiDesc(name = "description", type = "string", desc = "备注"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/sysset/profitDist/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param ProfitDist profitDist) {
        profitDist.setState(1);
        profitDistDao.add(profitDist);
        return new Result(true);
    }

    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
            @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
            @ApiDesc(name = "total", type = "string", desc = "总收益"),
            @ApiDesc(name = "mStartTime", type = "string", desc = "经营权范围开始"),
            @ApiDesc(name = "mEndTime", type = "string", desc = "经营券范围结束"),
            @ApiDesc(name = "description", type = "string", desc = "备注"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/profitDist/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param ProfitDist profitDist) {
        profitDistDao.update(profitDist);
        return new Result(true);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "profitDistId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/profitDist/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param ProfitDist profitDist) {
        profitDistDao.del(profitDist);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
                    @ApiDesc(name = "total", type = "string", desc = "总收益"),
                    @ApiDesc(name = "mStartTime", type = "string", desc = "经营权范围开始"),
                    @ApiDesc(name = "mEndTime", type = "string", desc = "经营券范围结束"),
                    @ApiDesc(name = "description", type = "string", desc = "备注"),
            }
    )
    @Router(path = "/sysset/profitDist/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param ProfitDist profitDist) {
        return new Result(profitDistDao.list(profitDist, ProfitDist.class));
    }

    @ApiDoc(title = "分页查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
                    @ApiDesc(name = "total", type = "string", desc = "总收益"),
                    @ApiDesc(name = "mStartTime", type = "string", desc = "经营权范围开始"),
                    @ApiDesc(name = "mEndTime", type = "string", desc = "经营券范围结束"),
                    @ApiDesc(name = "description", type = "string", desc = "备注"),
            }
    )
    @Router(path = "/sysset/profitDist/NaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result naviList(RestForm form, @Param ProfitDist profitDist) {
        return profitDistDao.naviList(profitDist, form);
    }


    @ApiDoc(title = "查询分润详情", param = {
            @ApiDesc(name = "profitDistId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "startTime", type = "string", desc = "开始时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "结束时间"),
                    @ApiDesc(name = "total", type = "string", desc = "总收益"),
                    @ApiDesc(name = "mStartTime", type = "string", desc = "经营权范围开始"),
                    @ApiDesc(name = "mEndTime", type = "string", desc = "经营券范围结束"),
                    @ApiDesc(name = "description", type = "string", desc = "备注"),
            }
    )
    @Router(path = "/sysset/profitDist/naviDetialList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result naviDetialList(RestForm form, @Param ProfitDist profitDist) {
        return profitDistDao.naviDetialList(profitDist, form);
    }


    @ApiDoc(title = "标记状态为待处理", param = {
            @ApiDesc(name = "ProfitDistId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/ProfitDist/tagState-1")
    @Proxy(target = AuthProxy.class)
    public Result tagState(RestForm form, @Param ProfitDist profitDist) {
        ProfitDist param = new ProfitDist();
        param.setProfitDistId(profitDist.getProfitDistId());
        param.setState(1);
        profitDistDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为处理中", param = {
            @ApiDesc(name = "ProfitDistId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/ProfitDist/tagState-2")
    @Proxy(target = AuthProxy.class)
    public Result tagState2(RestForm form, @Param ProfitDist profitDist) {
        ProfitDist param = new ProfitDist();
        param.setProfitDistId(profitDist.getProfitDistId());
        param.setState(2);
        profitDistDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为完成", param = {
            @ApiDesc(name = "profitDistId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/sysset/ProfitDist/tagState-3")
    @Proxy(target = AuthProxy.class)
    public Result tagState3(RestForm form, @Param ProfitDist profitDist) {
        ProfitDist param = new ProfitDist();
        param.setProfitDistId(profitDist.getProfitDistId());
        param.setState(3);
        profitDistDao.update(param);
        profitDistDao.insertBill(param);
        return new Result(true);
    }


}
