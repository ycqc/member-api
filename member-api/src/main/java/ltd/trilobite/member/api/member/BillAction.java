package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;


import ltd.trilobite.member.dao.BillDao;
import ltd.trilobite.member.dao.PersonDao;
import ltd.trilobite.member.dao.entry.Bill;
import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.ArrayList;
import java.util.List;

@ApiDesc(desc = "3003-会员账单")
@RestService
public class BillAction {


    BillDao billDao = new BillDao();
    PersonDao dao = new PersonDao();

//    @ApiDoc(title = "新增", param = {
//
//            @ApiDesc(name = "orderId", type = "string", desc = "编号"),
//            @ApiDesc(name = "personId", type = "string", desc = "权益"),
//            @ApiDesc(name = "memberProductId", type = "header", desc = "费用"),
//
//            @ApiDesc(name = "token", type = "header", desc = "密匙")
//    },
//            result = {
//                    @ApiDesc(name = "true", type = "string", desc = "成功"),
//            }
//
//    )
//    @Router(path = "/member/bill/Add")
//    @Proxy(target = AuthProxy.class)
//    public Result add(RestForm form, @Param Bill bill) {
//
//        billDao.add(bill);
//        return new Result(true);
//    }

//    @ApiDoc(title = "删除", param = {
//            @ApiDesc(name = "billId", type = "string", desc = "编号"),
//            @ApiDesc(name = "token", type = "header", desc = "密匙"),
//    },
//            result = {
//                    @ApiDesc(name = "true", type = "string", desc = "成功"),
//            }
//    )
//    @Router(path = "/member/bill/Delete")
//    @Proxy(target = AuthProxy.class)
//    public Result delete(RestForm form, @Param Bill bill) {
//        billDao.del(bill);
//        return new Result(true);
//    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "billId", type = "string", desc = "编号"),
                    @ApiDesc(name = "name", type = "string", desc = "权益"),
                    @ApiDesc(name = "price", type = "header", desc = "费用"),
            }
    )
    @Router(path = "/member/bill/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param Bill bill) {
        return new Result(billDao.list(bill, Bill.class));
    }

    @ApiDoc(title = "我的账单分页查询", param = {

            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "lable", type = "string", desc = "收益类型"),
                    @ApiDesc(name = "state", type = "string", desc = "状态"),
                    @ApiDesc(name = "description", type = "string", desc = "备注"),
                    @ApiDesc(name = "num", type = "string", desc = "收益数量"),
                    @ApiDesc(name = "createTime", type = "string", desc = "收益时间"),


            }
    )
    @Router(path = "/member/bill/findMyNaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findMyNaviList(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return billDao.findMyNaviList(personId, form);
    }

    @ApiDoc(title = "账单分页查询", param = {

            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "lable", type = "string", desc = "收益类型"),
                    @ApiDesc(name = "state", type = "string", desc = "状态"),
                    @ApiDesc(name = "description", type = "string", desc = "备注"),
                    @ApiDesc(name = "num", type = "string", desc = "收益数量"),
                    @ApiDesc(name = "createTime", type = "string", desc = "收益时间"),


            }
    )
    @Router(path = "/member/bill/NaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result NaviList(RestForm form) {
        System.out.println(form.getParam());
        return billDao.findNaviList(form);
    }


    @ApiDoc(title = "查询余额", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "data", type = "string", desc = "余额"),
            }
    )
    @Router(path = "/member/bill/findBalance", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findBalance(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(billDao.findBalance(personId));
    }


    @ApiDoc(title = "查询余额", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "data", type = "string", desc = "余额"),
            }
    )
    @Router(path = "/member/bill/transferAccounts", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result transferAccounts(RestForm form, @Param Bill bill, @Param Person person) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));

        if (!dao.hasOldPayPass(person.getPayPass(), personId)) {
            return new Result(-3);//支付密码不正确
        }
        if (bill.getNum() > billDao.findBalance(personId)) {
            return new Result(-1);//转账不能超过自己的余额
        }

        Person param = new Person();
        param.setPersonId(personId);
        Person param1 = new Person();
        param1.setPhoneNum(person.getPhoneNum());
        Person targetPerson = dao.findOne(param1, Person.class);
        Person my = dao.findOne(param, Person.class);
        if (targetPerson.getPersonId() == personId) {
            return new Result(-2);//不能转自己的账户
        }

        Bill myBill = new Bill();
        myBill.setPersonId(personId);
        myBill.setNum(-bill.getNum());
        myBill.setCode("5");
        myBill.setActionCode("8");//转出
        myBill.setState(3);
        myBill.setSrcId(targetPerson.getPersonId());
        myBill.setDescription("转出到账号：" + targetPerson.getPhoneNum() + "[" + targetPerson.getName() + "]");

        Bill targetBill = new Bill();
        targetBill.setPersonId(targetPerson.getPersonId());
        targetBill.setNum(bill.getNum());
        targetBill.setCode("5");
        targetBill.setActionCode("7");//转入
        targetBill.setState(3);
        targetBill.setSrcId(my.getPersonId());
        targetBill.setDescription("来自账号：" + my.getPhoneNum() + "[" + my.getName() + "]");
        List<Object> adds = new ArrayList<>();
        adds.add(myBill);
        adds.add(targetBill);
        billDao.adds(adds);

        return new Result(200);
    }


    @ApiDoc(title = "标记状态为待处理", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/bill/tagState-1")
    @Proxy(target = AuthProxy.class)
    public Result tagState(RestForm form, @Param Bill bill) {
        Bill param = new Bill();
        param.setBillId(bill.getBillId());
        param.setState(1);
        billDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为处理中", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/bill/tagState-2")
    @Proxy(target = AuthProxy.class)
    public Result tagState2(RestForm form, @Param Bill bill) {
        Bill param = new Bill();
        param.setBillId(bill.getBillId());
        param.setState(2);
        billDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "标记状态为完成", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/bill/tagState-3")
    @Proxy(target = AuthProxy.class)
    public Result tagState3(RestForm form, @Param Bill bill) {
        Bill param = new Bill();
        param.setBillId(bill.getBillId());
        param.setState(3);
        billDao.update(param);
        return new Result(true);
    }

    @ApiDoc(title = "查询我的账单统计", param = {
            @ApiDesc(name = "billId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "×", type = "string", desc = ""),
            }
    )
    @Router(path = "/member/bill/findMyBillCount")
    @Proxy(target = AuthProxy.class)
    public Result findMyBillCount(RestForm form, @Param Bill bill) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));

        return billDao.findMyBillCount(personId);
    }


}
