package ltd.trilobite.member.api.notify;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.member.dao.NotifyDao;
import ltd.trilobite.member.dao.entry.Notify;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.Date;

@ApiDesc(desc = "1001-通知公告")
@RestService
public class NotifyAction {
    NotifyDao notifyDao = new NotifyDao();

    @ApiDoc(title = "保存", param = {

            @ApiDesc(name = "notifyId", type = "string", desc = "编号"),
            @ApiDesc(name = "message", type = "string", desc = "内容"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "200-成功 -1-失败"),
            }

    )
    @Router(path = "/notify/save")
    public Result save(RestForm form, @Param Notify notify) {
        if(notify.getNotifyId() == null){
            notifyDao.updateAllMessasgeStateToOff();
            notify.setMessageState(1);
            notify.setCreateTime(new Date());
            notifyDao.add(notify);
        }else{
            notifyDao.update(notify);
        }
        return new Result(200);
    }

    @ApiDoc(title = "后台-分页查询", param = {
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "notifyId", type = "string", desc = "编号"),
                    @ApiDesc(name = "message", type = "string", desc = "内容"),
                    @ApiDesc(name = "messageState", type = "string", desc = "状态"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }

    )
    @Router(path = "/notify/list")
    public Result findNaviList(RestForm form) {
        return notifyDao.findNaviList(form);
    }

    @ApiDoc(title = "前端-获取最新", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "message", type = "string", desc = "内容"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            }

    )
    @Router(path = "/notify/new")
    public Result findNew(RestForm form) {
        return new Result(notifyDao.findNew());
    }


    @ApiDoc(title = "启用", param = {

            @ApiDesc(name = "notifyId", type = "string", desc = "编号"),
            @ApiDesc(name = "message", type = "string", desc = "权益"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "200-成功 -1-失败"),
            }

    )
    @Router(path = "/notify/on")
    public Result on(RestForm form, @Param Notify notify) {
        notifyDao.updateAllMessasgeStateToOff();
        notifyDao.updateMessageState(notify.getNotifyId(), 1);
        return new Result(200);
    }

    @ApiDoc(title = "禁用", param = {

            @ApiDesc(name = "notifyId", type = "string", desc = "编号"),
            @ApiDesc(name = "message", type = "string", desc = "权益"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "200-成功 -1-失败"),
            }

    )
    @Router(path = "/notify/off")
    public Result off(RestForm form, @Param Notify notify) {
        notifyDao.updateMessageState(notify.getNotifyId(), 2);
        return new Result(200);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "notifyId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "200-成功 -1-失败"),
            }

    )
    @Router(path = "/notify/delete")
    public Result delete(RestForm form, @Param Notify notify) {
        notifyDao.deleteById(notify.getNotifyId());
        return new Result(200);
    }
}
