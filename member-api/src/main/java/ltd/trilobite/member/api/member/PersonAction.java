package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.common.MD5Util;

import ltd.trilobite.member.dao.PersonDao;
import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.member.service.SmsService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1002-账号管理")
@RestService
public class PersonAction {


    PersonDao personDao = new PersonDao();

    SmsService smsService=new SmsService();

    @ApiDoc(title = "注册", param = {
            @ApiDesc(name = "gender", type = "string", desc = "性别"),
            @ApiDesc(name = "pass", type = "string", desc = "密码"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
            @ApiDesc(name = "vcode", type = "string", desc = "验证码"),

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )

    @Router(path = "/member/person/Add")
    public Result add(RestForm form, @Param Person person) {
        Result rs = new Result(200);
        int code=smsService.valiCode(person.getPhoneNum(),form.get("vcode"));
        if(code==-1){
            rs.setCode(-1);
            return rs;
        }
        if (validateUser(person.getPhoneNum())) {
            rs.setCode(-3);
        } else {
            person.setPass(MD5Util.md5(person.getPass()));
            personDao.add(person);
            rs = new Result(true);
        }
        return rs;
    }

    private Boolean validateUser(String username) {
        PersonDao personDao = new PersonDao();
        Person person = new Person();
        person.setPhoneNum(username);
        return personDao.findOne(person, Person.class) != null;
    }

    @ApiDoc(title = "修改密码", param = {
            @ApiDesc(name = "oldPass", type = "string", desc = "编号"),
            @ApiDesc(name = "pass", type = "string", desc = "最小级别"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/person/myUpdatePass")
    @Proxy(target = AuthProxy.class)
    public Result myUpdatePass(RestForm form, @Param Person person) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        if (personDao.hasOldPass(form.get("oldPass"), personId)) {
            Person ps = new Person();
            ps.setPersonId(personId);
            ps.setPass(MD5Util.md5(person.getPass()));
            personDao.update(ps);
        } else {
            return new Result(-1);
        }
        return new Result(200);
    }


    @ApiDoc(title = "修改支付密码", param = {
            @ApiDesc(name = "oldPass", type = "string", desc = "旧密码"),
            @ApiDesc(name = "payPass", type = "string", desc = "新密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/person/myUpdatePayPass")
    @Proxy(target = AuthProxy.class)
    public Result myUpdatePayPass(RestForm form, @Param Person person) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        if (!personDao.oldPlayPassIsNotNull(personId) || personDao.hasOldPayPass(form.get("oldPass"), personId)) {
            Person ps = new Person();
            ps.setPersonId(personId);
            ps.setPayPass(MD5Util.md5(person.getPayPass()));
            personDao.update(ps);
        } else {
            return new Result(-1);
        }
        return new Result(200);
    }

    @ApiDoc(title = "是否设置支付密码", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/person/isSetPayPasss")
    @Proxy(target = AuthProxy.class)
    public Result isSetPayPass(RestForm form, @Param Person person) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(personDao.oldPlayPassIsNotNull(personId));
    }

    @ApiDoc(title = "修改个人资料", param = {
            @ApiDesc(name = "name", type = "string", desc = "姓名"),
            @ApiDesc(name = "gender", type = "string", desc = "性别"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/person/myUpdate")
    @Proxy(target = AuthProxy.class)
    public Result myUpdate(RestForm form, @Param Person person) {
        person.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        person.setLockState(null);
        person.setPayPass(null);
        person.setPass(null);
        person.setPhoneNum(null);
//        person.setLevelNum(null);
        personDao.update(person);
        return new Result(true);
    }


    @ApiDoc(title = "个人信息加载", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
                    @ApiDesc(name = "frontCard", type = "string", desc = "身份整正面"),
                    @ApiDesc(name = "backCard", type = "string", desc = "身份整背面"),

                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )

    @Router(path = "/member/person/Info")
    @Proxy(target = AuthProxy.class)
    public Result myInfo(RestForm form, @Param Person person) {
        person.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        Object result = personDao.myInfo(person);
        return new Result(result);
    }


    @ApiDoc(title = "设置会员类型为初始类型", param = {
            @ApiDesc(name = "personId", type = "string", desc = "人员编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "20", type = "string", desc = "成功"),
            }
    )

    @Router(path = "/member/person/setMemberType-1")
    @Proxy(target = AuthProxy.class)
    public Result setMemberType(RestForm form, @Param Person person) {
        Person param = new Person();
        param.setPersonId(person.getPersonId());
        param.setMemberType(1);
        personDao.update(param);
        return new Result(200);
    }

    @ApiDoc(title = "加锁", param = {
            @ApiDesc(name = "personId", type = "string", desc = "人员编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "20", type = "string", desc = "成功"),
            }
    )

    @Router(path = "/member/person/lock")
    @Proxy(target = AuthProxy.class)
    public Result lock(RestForm form, @Param Person person) {
        Person param = new Person();
        param.setPersonId(person.getPersonId());
        param.setLockState(1);
        personDao.update(param);
        return new Result(200);
    }

    @ApiDoc(title = "加锁", param = {
            @ApiDesc(name = "personId", type = "string", desc = "人员编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "20", type = "string", desc = "成功"),
            }
    )

    @Router(path = "/member/person/unLock")
    @Proxy(target = AuthProxy.class)
    public Result unlock(RestForm form, @Param Person person) {
        Person param = new Person();
        param.setPersonId(person.getPersonId());
        param.setLockState(2);
        personDao.update(param);
        return new Result(200);
    }

    @ApiDoc(title = "根据手机号查询人员信息", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),

                    @ApiDesc(name = "token", type = "header", desc = "密匙")
            }
    )
    @Router(path = "/member/person/findAccount")
    @Proxy(target = AuthProxy.class)
    public Result findAccount(RestForm form, @Param Person person) {

        Person param = new Person();
        param.setPhoneNum(person.getPhoneNum());
        Object result = personDao.findOne(param, Person.class);
        return new Result(result);
    }


    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "personId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/person/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param Person person) {
        personDao.del(person);
        return new Result(true);
    }

    @ApiDoc(title = "单表查询", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "personId", type = "string", desc = "编号"),
                    @ApiDesc(name = "minLevel", type = "string", desc = "最小级别"),
                    @ApiDesc(name = "maxLevel", type = "string", desc = "最大级别"),
                    @ApiDesc(name = "name", type = "string", desc = "称号"),
            }
    )
    @Router(path = "/member/person/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param Person person) {
        return new Result(personDao.list(person, Person.class));
    }

    @ApiDoc(title = "分页查询", param = {
            @ApiDesc(name = "name", type = "string", desc = "姓名"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "name", type = "string", desc = "姓名"),
                    @ApiDesc(name = "gender", type = "string", desc = "性别"),
                    @ApiDesc(name = "userName", type = "string", desc = "用户名"),
                    @ApiDesc(name = "pass", type = "string", desc = "密码"),
                    @ApiDesc(name = "isAuth", type = "string", desc = "是否实名认证"),
                    @ApiDesc(name = "fnCount", type = "string", desc = "授权功能数量"),
                    @ApiDesc(name = "phoneNum", type = "string", desc = "手机号码"),

            }
    )
    @Router(path = "/member/person/NaviList")
    @Proxy(target = AuthProxy.class)
    public Result naviList(RestForm form, @Param Person person) {
        return personDao.findNaviList(person, form);
    }
}
