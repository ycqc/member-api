package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.member.dao.PersonBankCardDao;
import ltd.trilobite.member.dao.entry.PersonBankCard;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3005-我的银行卡")
@RestService
public class PersonBankCardAction {


    PersonBankCardDao personBankCardDao = new PersonBankCardDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "num", type = "string", desc = "卡号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/member/personBankCard/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param PersonBankCard personBankCard) {
        personBankCard.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        personBankCardDao.add(personBankCard);
        return new Result(200);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "personBankCardId", type = "string", desc = "编号"),
            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "num", type = "string", desc = "卡号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/personBankCard/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param PersonBankCard personBankCard) {
        personBankCard.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        personBankCardDao.update(personBankCard);
        return new Result(200);
    }

    @ApiDoc(title = "查询银行卡信息", param = {

            @ApiDesc(name = "personBankCardId", type = "string", desc = "编号"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/personBankCard/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param PersonBankCard personBankCard) {
        personBankCard.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        personBankCard.setPersonBankCardId(personBankCard.getPersonBankCardId());
        return new Result(personBankCardDao.findOne(personBankCard, PersonBankCard.class));
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "personBankCardId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/personBankCard/Delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param PersonBankCard personBankCard) {
        personBankCard.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        if (personBankCardDao.findOne(personBankCard, PersonBankCard.class) != null) {
            personBankCardDao.del(personBankCard);
        } else {
            return new Result(-1);
        }

        return new Result(200);

    }

    @ApiDoc(title = "单表查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "personBankCardId", type = "string", desc = "编号"),
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
                    @ApiDesc(name = "num", type = "string", desc = "卡号"),
            }
    )
    @Router(path = "/member/personBankCard/List", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form) {
        PersonBankCard personBankCard = new PersonBankCard();
        personBankCard.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        return new Result(personBankCardDao.list(personBankCard, PersonBankCard.class));
    }
}
