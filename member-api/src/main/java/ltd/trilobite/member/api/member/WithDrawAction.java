package ltd.trilobite.member.api.member;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;


import ltd.trilobite.member.common.CertificationConstant;
import ltd.trilobite.member.dao.*;
import ltd.trilobite.member.dao.entry.*;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3006-提现")
@RestService
public class WithDrawAction {
    //提现扣除金额百分比设置
    private static final int WITHDRAW_DEDUCT_GLOBAL_SET_ID = 8;
    //提现扣除金额百分比设置
    private static final int WITHDRAW_MIN_GLOBAL_SET_ID = 10;
    //积分
    private static final String SCORE_CODE = "5";
    //提现动作
    private static final String WITHDRAW_ACTIION_CODE = "6";
    //循环钱包
    private static final String BALANCE_CODE = "6";
    //提现扣除金额用于重复消费
    private static final String WITHDRAW_DEDUCT_ACTIION_CODE = "11";

    //待处理
    private static final int STATE_PENDING = 1;
    //处理中
    private static final int STATE_PROCESSING = 2;
    //已完成
    private static final int STATE_COMPLETE = 3;
    //提现成功
    private static final int WITHDRAW_SUCCESS = 1;
    //提现失败
    private static final int WITHDRAW_FAIL = 2;

    PersonDao personDao = new PersonDao();
    BillDao billDao = new BillDao();
    WithdrawDao withdrawDao = new WithdrawDao();
    GlobalSetDao globalSetDao = new GlobalSetDao();
    PersonCertificationDao personCertificationDao = new PersonCertificationDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "num", type = "string", desc = "提取金额"),
            @ApiDesc(name = "state", type = "string", desc = "提取金额"),
            @ApiDesc(name = "personId", type = "string", desc = "提取金额"),
            @ApiDesc(name = "auditPerson", type = "string", desc = "提取金额"),
            @ApiDesc(name = "message", type = "string", desc = "提现说明"),
            @ApiDesc(name = "bankName", type = "string", desc = "银行名"),
            @ApiDesc(name = "bankAccount", type = "string", desc = "账户名"),
            @ApiDesc(name = "bankNum", type = "string", desc = "提取金额"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "-1-积分不足 -2-未实名认证 -3支付密码错误 -4:提现金额不能小于系统设置最小提现金额"),
            }

    )
    @Router(path = "/member/withdraw/Add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param Withdraw withdraw, @Param Person person) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));

        PersonCertification personCertification = personCertificationDao.findById(personId);
        if(!(personCertification != null
                && personCertification.getAuditState() == CertificationConstant.STATE_CHECKED
                && personCertification.getIsAudit() == CertificationConstant.AUDIT_OK)){
            return new Result(-2);//未实名认证
        }

        if (!personDao.hasOldPayPass(person.getPayPass(), personId)) {
            return new Result(-3);//支付密码不正确
        }

        Double minNum = Double.parseDouble(globalSetDao.findById(WITHDRAW_MIN_GLOBAL_SET_ID).getConstVal());
        if(withdraw.getNum() < minNum){ //提现金额不能小于系统设置最小提现金额
            return new Result(-4);
        }

        Double balance = billDao.findBalance(Long.parseLong(form.getHeader().get("personId")));
        if (withdraw.getNum() > balance) {
            return new Result(-1);//提现不能超过自己的余额
        }

        withdraw.setWithdrawId(withdrawDao.getSeq("withdraw"));
        withdraw.setPersonId(personId);
        withdraw.setState(1);
        withdrawDao.add(withdraw);

        Double deductProportion = Double.parseDouble(globalSetDao.findById(WITHDRAW_DEDUCT_GLOBAL_SET_ID).getConstVal());
        if(deductProportion > 0){
            Double deductNum = (deductProportion *  withdraw.getNum()) / 100d;
            Double withdrawNum = withdraw.getNum() - deductNum;

            Bill bill = new Bill();
            bill.setNum(-withdrawNum);
            bill.setCode(SCORE_CODE);
            bill.setPersonId(withdraw.getPersonId());
            bill.setState(STATE_PENDING);
            bill.setSrcId(withdraw.getWithdrawId());
            bill.setDescription("安全提现");
            bill.setActionCode(WITHDRAW_ACTIION_CODE);
            billDao.add(bill);

            Bill balanceBill = new Bill();
            balanceBill.setNum(-deductNum);
            balanceBill.setCode(SCORE_CODE);
            balanceBill.setPersonId(withdraw.getPersonId());
            balanceBill.setState(STATE_PENDING);
            balanceBill.setSrcId(withdraw.getWithdrawId());
            balanceBill.setDescription("提现手续费");
            balanceBill.setActionCode(WITHDRAW_ACTIION_CODE);
            billDao.add(balanceBill);
        }

        return new Result(200);
    }

    @ApiDoc(title = "修改", param = {

            @ApiDesc(name = "withdrawId", type = "string", desc = "编号"),
            @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
            @ApiDesc(name = "num", type = "string", desc = "卡号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }
    )
    @Router(path = "/member/withdraw/Update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param Withdraw withdraw) {
        withdraw.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        Double balance = billDao.findBalance(Long.parseLong(form.getHeader().get("personId")));
        if (withdraw.getNum() > balance) {
            return new Result(-1);//提现不能超过自己的余额
        }
        withdrawDao.update(withdraw);
        return new Result(200);
    }

    @ApiDoc(title = "查询提现信息", param = {
            @ApiDesc(name = "withdrawId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名称"),
                    @ApiDesc(name = "num", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "state", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "personId", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "auditPerson", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "message", type = "string", desc = "提现说明"),
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名"),
                    @ApiDesc(name = "bankAccount", type = "string", desc = "账户名"),
                    @ApiDesc(name = "bankNum", type = "string", desc = "提取金额"),
            }
    )
    @Router(path = "/member/withdraw/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param Withdraw withdraw) {
        withdraw.setPersonId(Long.parseLong(form.getHeader().get("personId")));
        withdraw.setWithdrawId(withdraw.getWithdrawId());
        return new Result(withdrawDao.findOne(withdraw, Withdraw.class));
    }

    @ApiDoc(title = "分页列表", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {

                    @ApiDesc(name = "withdrawId", type = "string", desc = "提现ID"),
                    @ApiDesc(name = "num", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "state", type = "string", desc = "状态 1-待处理 2-处理中 3-已完成"),
                    @ApiDesc(name = "isSuccess", type = "string", desc = "是否成功 1-成功 2-失败"),
                    @ApiDesc(name = "bankAccount", type = "string", desc = "账户名"),
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名"),
                    @ApiDesc(name = "bankNum", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "auditPersonName", type = "string", desc = "审核人"),
                    @ApiDesc(name = "message", type = "string", desc = "提现说明"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "完成时间"),
            }
    )
    @Router(path = "/member/withdraw/myNaviList", method = Router.Action.Get)
    @Proxy(target = AuthProxy.class)
    public Result findMyNaviList(RestForm form) {
        return withdrawDao.findMyNaviList(form, Long.parseLong(form.getHeader().get("personId")));
    }



    @ApiDoc(title = "[后台]分页列表", param = {
            @ApiDesc(name = "personName", type = "string", desc = "提现人"),
            @ApiDesc(name = "phoneNum", type = "string", desc = "手机号"),
            @ApiDesc(name = "bankAccount", type = "string", desc = "持卡人"),
            @ApiDesc(name = "bankNum", type = "string", desc = "提取金额"),
            @ApiDesc(name = "state", type = "string", desc = "状态 1-待处理 2-处理中 3-已完成"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "pageSize", type = "string", desc = "页面大小"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "withdrawId", type = "string", desc = "提现ID"),
                    @ApiDesc(name = "num", type = "string", desc = "提取金额"),
                    @ApiDesc(name = "state", type = "string", desc = "状态 1-待处理 2-处理中 3-已完成"),
                    @ApiDesc(name = "isSuccess", type = "string", desc = "是否成功 1-成功 2-失败"),
                    @ApiDesc(name = "personId", type = "string", desc = "提现人ID"),
                    @ApiDesc(name = "personName", type = "string", desc = "提现人姓名"),
                    @ApiDesc(name = "realName", type = "string", desc = "实名认证姓名"),
                    @ApiDesc(name = "cardNo", type = "string", desc = "身份证号"),
                    @ApiDesc(name = "bankAccount", type = "string", desc = "账户名"),
                    @ApiDesc(name = "bankName", type = "string", desc = "银行名"),
                    @ApiDesc(name = "bankNum", type = "string", desc = "银行卡号"),
                    @ApiDesc(name = "auditPerson", type = "string", desc = "审核人ID"),
                    @ApiDesc(name = "auditPersonName", type = "string", desc = "审核人姓名"),
                    @ApiDesc(name = "message", type = "string", desc = "备注"),
                    @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
                    @ApiDesc(name = "endTime", type = "string", desc = "完成时间"),
            }
    )
    @Router(path = "/member/withdraw/list")
    @Proxy(target = AuthProxy.class)
    public Result list(RestForm form) {
        return withdrawDao.findNaviList(form);
    }


    @ApiDoc(title = "设置提现处理中", param = {
            @ApiDesc(name = "withdrawId", type = "string", desc = "提现ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
            }
    )
    @Router(path = "/member/withdraw/stateToProcessing")
    @Proxy(target = AuthProxy.class)
    public Result stateToProcessing(RestForm form) {
        Long auditPerson = Long.parseLong(form.getHeader().get("personId"));
        Withdraw withdraw = new Withdraw();
        withdraw.setWithdrawId(Long.parseLong(form.get("withdrawId")));
        withdraw = withdrawDao.findOne(withdraw, Withdraw.class);
        if(withdraw.getState() == STATE_PENDING) {
            withdrawDao.updateState(withdraw.getWithdrawId(), STATE_PROCESSING, auditPerson);
            billDao.updateState(withdraw.getPersonId(), withdraw.getWithdrawId(), SCORE_CODE, WITHDRAW_ACTIION_CODE, STATE_PROCESSING);
            billDao.updateState(withdraw.getPersonId(), withdraw.getWithdrawId(), BALANCE_CODE, WITHDRAW_DEDUCT_ACTIION_CODE, STATE_PROCESSING);
            return new Result(200);
        }else{
            return new Result(-1);
        }
    }

    @ApiDoc(title = "设置提现完成", param = {
            @ApiDesc(name = "withdrawId", type = "string", desc = "提现ID"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
            }
    )
    @Router(path = "/member/withdraw/stateToComplete")
    @Proxy(target = AuthProxy.class)
    public Result stateToComplete(RestForm form) {
        Long auditPerson = Long.parseLong(form.getHeader().get("personId"));
        Withdraw withdraw = withdrawDao.findById(Long.parseLong(form.get("withdrawId")));
        if(withdraw.getState() == STATE_PENDING || withdraw.getState() == STATE_PROCESSING) {
            withdrawDao.updateState(withdraw.getWithdrawId(), STATE_COMPLETE, auditPerson);
            billDao.updateState(withdraw.getPersonId(), withdraw.getWithdrawId(), SCORE_CODE, WITHDRAW_ACTIION_CODE, STATE_COMPLETE);
            billDao.updateState(withdraw.getPersonId(), withdraw.getWithdrawId(), BALANCE_CODE, WITHDRAW_DEDUCT_ACTIION_CODE, STATE_COMPLETE);
            return new Result(200);
        }else{
            return new Result(-1);
        }
    }

    @ApiDoc(title = "驳回提现", param = {
            @ApiDesc(name = "withdrawId", type = "string", desc = "提现ID"),
            @ApiDesc(name = "message", type = "string", desc = "备注"),
            @ApiDesc(name = "token", type = "header", desc = "密匙"),
    },
            result = {
                    @ApiDesc(name = "code", type = "string", desc = "返回码 -1-失败 200-成功"),
            }
    )
    @Router(path = "/member/withdraw/rollback")
    @Proxy(target = AuthProxy.class)
    public Result setFail(RestForm form) {
        Withdraw withdraw = withdrawDao.findById(Long.parseLong(form.get("withdrawId")));
        if (withdraw.getState() == STATE_PENDING || withdraw.getState() == STATE_PROCESSING) {
            withdrawDao.updateIsSuccess(withdraw.getWithdrawId(), WITHDRAW_FAIL, form.get("message"));
            return new Result(200);
        } else {
            return new Result(-1);
        }
    }
}
