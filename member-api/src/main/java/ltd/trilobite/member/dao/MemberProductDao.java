package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.MemberProduct;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;
import java.util.Map;

public class MemberProductDao extends BaseDao<MemberProduct> {
    public List<Map<String, Object>> myProductCount(MemberProduct memberProduct, long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select  mp.member_product_id,mp.name,m2.count,m2.count*mp.price as price from member_product mp left join\n" +
                "            (select member_product_id,count(1) from  member_order where person_id=" + personId + " group by member_product_id)\n" +
                "    m2 on mp.member_product_id = m2.member_product_id where mp.on_off=0";
        return jdbcTemplet.findList(sql);
    }
}
