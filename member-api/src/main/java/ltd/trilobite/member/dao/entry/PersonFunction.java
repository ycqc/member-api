package ltd.trilobite.member.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_function")
public class PersonFunction {

  @Id(type = IdType.Seq)
  private Long personFunctionId;
  private Long personId;
  private Long functionId;


  public Long getPersonFunctionId() {
    return personFunctionId;
  }

  public void setPersonFunctionId(Long personFunctionId) {
    this.personFunctionId = personFunctionId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Long getFunctionId() {
    return functionId;
  }

  public void setFunctionId(Long functionId) {
    this.functionId = functionId;
  }

}
