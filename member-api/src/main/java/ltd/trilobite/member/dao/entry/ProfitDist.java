package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "profit_dist")
public class ProfitDist {
    @Id(type = IdType.Seq)
    private Long profitDistId;
    private java.sql.Timestamp startTime;
    private java.sql.Timestamp endTime;
    private String description;
    private Double total;
    private java.sql.Timestamp mStartTime;
    private java.sql.Timestamp mEndTime;
    private Integer state;


    public Long getProfitDistId() {
        return profitDistId;
    }

    public void setProfitDistId(Long profitDistId) {
        this.profitDistId = profitDistId;
    }


    public java.sql.Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(java.sql.Timestamp startTime) {
        this.startTime = startTime;
    }


    public java.sql.Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(java.sql.Timestamp endTime) {
        this.endTime = endTime;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }


    public java.sql.Timestamp getMStartTime() {
        return mStartTime;
    }

    public void setMStartTime(java.sql.Timestamp mStartTime) {
        this.mStartTime = mStartTime;
    }


    public java.sql.Timestamp getMEndTime() {
        return mEndTime;
    }

    public void setMEndTime(java.sql.Timestamp mEndTime) {
        this.mEndTime = mEndTime;
    }


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
