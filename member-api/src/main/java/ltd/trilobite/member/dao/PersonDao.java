package ltd.trilobite.member.dao;

import ltd.trilobite.common.MD5Util;

import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PersonDao extends BaseDao<Person> {
    public Result findNaviList(Person person, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL countsql=new SQL();

        SQL personSql = new SQL();
        personSql.select().cols("person_id","name","gender","phone_num","tream_num","tream_park","member_type","to_char(create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time",
                "(select count(1) from person_function where person_id=p.person_id) as fn_count")
                .from().t("person p");
        personSql.where(true);
        List<Object> param=new ArrayList<Object>();

        if(Util.isNotEmpty(person.getName())) {
            personSql.and(new SqlParm("name").like().v("?"));
            param.add("%"+person.getName()+"%");
        }

        if(Util.isNotEmpty(person.getPhoneNum())) {
            personSql.and(new SqlParm("phone_num").eq().v("?"));
            param.add(person.getPhoneNum());
        }

        countsql.select().cols("count(1)").from(personSql.toString(),"a");

        personSql.order("person_id").desc();
        personSql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(personSql.toString(),countsql.toString(),null,param.toArray());

    }

    public Map<String, Object> myInfo(Person person) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getMap("select name,gender,phone_num,head_image_url,(select count(1)=1 from person_certification where person_id=? and is_audit=1 ) as is_certification from person where person_id=?"
                ,person.getPersonId(), person.getPersonId());
    }

    public boolean hasOldPass(String oldPass, Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        Person person=new Person();
        person.setPersonId(personId);
        person.setPass(MD5Util.md5(oldPass));
        return jdbcTemplet.one(person,Person.class)!=null;

    }

    public boolean hasOldPayPass(String oldPass, Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        Person person=new Person();
        person.setPersonId(personId);
        person.setPayPass(MD5Util.md5(oldPass));
        return jdbcTemplet.one(person,Person.class)!=null;
    }

    public boolean oldPlayPassIsNotNull(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        Person person=new Person();
        person.setPersonId(personId);
        return Util.isNotEmpty(jdbcTemplet.one(person,Person.class).getPayPass());
    }


}
