package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.Function;

public class FunctionDao extends BaseDao<Function> {
    public Function findById(Long functionId) {
        Function function =  new Function();
        function.setFunctionId(functionId);
        return findOne(function, Function.class);
    }
}
