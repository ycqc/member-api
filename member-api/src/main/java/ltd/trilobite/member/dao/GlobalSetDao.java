package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.GlobalSet;

public class GlobalSetDao extends BaseDao<GlobalSet> {
    public GlobalSet findById(Integer globalSetId) {
        GlobalSet globalSet = new GlobalSet();
        globalSet.setGlobalSetId(globalSetId);
        return findOne(globalSet, GlobalSet.class);
    }
}
