package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "component_bank")
public class ComponentBank {
    @Id(type = IdType.Seq)
    private Integer componentBankId;
    private String bankName;
    private String num;
    private String name;
    private String qrcodeImageUrl;

    public String getQrcodeImageUrl() {
        return qrcodeImageUrl;
    }

    public void setQrcodeImageUrl(String qrcodeImageUrl) {
        this.qrcodeImageUrl = qrcodeImageUrl;
    }




    public Integer getComponentBankId() {
        return componentBankId;
    }

    public void setComponentBankId(Integer componentBankId) {
        this.componentBankId = componentBankId;
    }


    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }


    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
