package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "performance_differ_config")
public class PerformanceDifferConfig {
    @Id
    private Long levelNum;
    private Long achievement;
    private Integer proportion;


    public Long getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(Long levelNum) {
        this.levelNum = levelNum;
    }


    public Long getAchievement() {
        return achievement;
    }

    public void setAchievement(Long achievement) {
        this.achievement = achievement;
    }


    public Integer getProportion() {
        return proportion;
    }

    public void setProportion(Integer proportion) {
        this.proportion = proportion;
    }

}
