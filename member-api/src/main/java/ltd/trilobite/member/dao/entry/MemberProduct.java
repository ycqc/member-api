package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "member_product")
public class MemberProduct {
    @Id(type = IdType.Seq)
    private Long memberProductId;
    private String name;
    private Long price;
    private Integer onOff;


    public Long getMemberProductId() {
        return memberProductId;
    }

    public void setMemberProductId(Long memberProductId) {
        this.memberProductId = memberProductId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }


    public Integer getOnOff() {
        return onOff;
    }

    public void setOnOff(Integer onOff) {
        this.onOff = onOff;
    }

}
