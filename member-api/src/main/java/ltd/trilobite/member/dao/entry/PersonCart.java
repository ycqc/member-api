package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_cart")
public class PersonCart {

    private Long cartProfitId;
    private Long personId;
    private Long cartInfoId;


    public Long getCartProfitId() {
        return cartProfitId;
    }

    public void setCartProfitId(Long cartProfitId) {
        this.cartProfitId = cartProfitId;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getCartInfoId() {
        return cartInfoId;
    }

    public void setCartInfoId(Long cartInfoId) {
        this.cartInfoId = cartInfoId;
    }

}
