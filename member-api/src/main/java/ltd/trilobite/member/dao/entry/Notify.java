package ltd.trilobite.member.dao.entry;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "notify")
public class Notify {

  @Id(type = IdType.Seq)
  private Long notifyId;
  private String message;
  private Integer messageState;
  private Date createTime;


  public Long getNotifyId() {
    return notifyId;
  }

  public void setNotifyId(Long notifyId) {
    this.notifyId = notifyId;
  }


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  public Integer getMessageState() {
    return messageState;
  }

  public void setMessageState(Integer messageState) {
    this.messageState = messageState;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

}
