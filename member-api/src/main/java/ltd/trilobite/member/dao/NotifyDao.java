package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.Notify;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NotifyDao extends BaseDao<Notify> {

    public void updateMessageState(Long notifyId, Integer messageState) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "update notify set message_state=? where notify_id=?";
        jdbcTemplet.execute(sql, messageState, notifyId);
    }

    public void updateAllMessasgeStateToOff() {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "update notify set message_state=2";
        jdbcTemplet.execute(sql);
    }

    public Result findNaviList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        SQL sql = new SQL();
        List<Object> params = new ArrayList<>();
        sql.select().cols("notify_id","message", "message_state", "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time")
                .from().t("notify").where(true);

        if(StringUtils.isNotBlank(form.get("messageState"))){
            sql.fix(" and message_state=?");
            params.add(Integer.parseInt(form.get("messageState")));
        }

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray());
    }

    public void deleteById(Long notifyId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "delete from notify where notify_id=?";
        jdbcTemplet.execute(sql, notifyId);
    }

    public Map<String, Object> findNew() {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select message,to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time from notify where message_state=1";
        return jdbcTemplet.getMap(sql);
    }
}
