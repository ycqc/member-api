package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "cart_info")
public class CartInfo {
    @Id(type = IdType.Seq)
    private Long cartInfoId;
    private String carNum;
    private String title;


    public Long getCartInfoId() {
        return cartInfoId;
    }

    public void setCartInfoId(Long cartInfoId) {
        this.cartInfoId = cartInfoId;
    }


    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
