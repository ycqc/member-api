package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.BaseData;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;
import java.util.Map;

public class BaseDataDao extends BaseDao<BaseData> {
    public List<Map<String, Object>> sortList(BaseData baseData) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.findList("select * from base_data where type_id=? order by sort_num asc", baseData.getTypeId());
    }
}
