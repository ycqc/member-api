package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "p_differ_week_bill")
public class PDifferWeekBill {

    private Long pDifferWeekBillId;
    private Long weekNum;
    private java.sql.Timestamp createTime;
    private Double amount;


    public Long getPDifferWeekBillId() {
        return pDifferWeekBillId;
    }

    public void setPDifferWeekBillId(Long pDifferWeekBillId) {
        this.pDifferWeekBillId = pDifferWeekBillId;
    }


    public Long getWeekNum() {
        return weekNum;
    }

    public void setWeekNum(Long weekNum) {
        this.weekNum = weekNum;
    }


    public java.sql.Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.sql.Timestamp createTime) {
        this.createTime = createTime;
    }


    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
