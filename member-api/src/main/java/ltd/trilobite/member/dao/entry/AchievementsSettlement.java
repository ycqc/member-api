package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;


@Table(name = "achievements_settlement")
public class AchievementsSettlement {
    @Id(type = IdType.Seq)
    private Long achievementsSettlementId;
    private java.sql.Timestamp startTime;
    private java.sql.Timestamp endTime;
    private Integer state;
    private String description;


    public Long getAchievementsSettlementId() {
        return achievementsSettlementId;
    }

    public void setAchievementsSettlementId(Long achievementsSettlementId) {
        this.achievementsSettlementId = achievementsSettlementId;
    }


    public java.sql.Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(java.sql.Timestamp startTime) {
        this.startTime = startTime;
    }


    public java.sql.Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(java.sql.Timestamp endTime) {
        this.endTime = endTime;
    }


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
