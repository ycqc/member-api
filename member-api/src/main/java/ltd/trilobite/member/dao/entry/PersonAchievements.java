package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_achievements")
public class PersonAchievements {
    @Id(type = IdType.Seq)
    private Long personAchievementsId;
    private Long num;
    private Long orderId;
    private java.sql.Timestamp createTime;
    private Long personId;
    private Long orderPersonId;


    public Long getPersonAchievementsId() {
        return personAchievementsId;
    }

    public void setPersonAchievementsId(Long personAchievementsId) {
        this.personAchievementsId = personAchievementsId;
    }


    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


    public java.sql.Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.sql.Timestamp createTime) {
        this.createTime = createTime;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getOrderPersonId() {
        return orderPersonId;
    }

    public void setOrderPersonId(Long orderPersonId) {
        this.orderPersonId = orderPersonId;
    }

}
