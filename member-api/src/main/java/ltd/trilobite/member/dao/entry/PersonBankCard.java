package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_bank_card")
public class PersonBankCard {
    @Id(type = IdType.Seq)
    private Long personBankCardId;
    private String bankName;
    private String num;
    private Long personId;
    private String name;


    public Long getPersonBankCardId() {
        return personBankCardId;
    }

    public void setPersonBankCardId(Long personBankCardId) {
        this.personBankCardId = personBankCardId;
    }


    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }


    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
