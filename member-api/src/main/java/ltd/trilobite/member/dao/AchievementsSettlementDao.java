package ltd.trilobite.member.dao;

import ltd.trilobite.common.dao.BaseDao;
import ltd.trilobite.member.dao.entry.AchievementsSettlement;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.ArrayList;
import java.util.List;

public class AchievementsSettlementDao extends BaseDao<AchievementsSettlement> {

    public Result navilist(RestForm form, AchievementsSettlement achievementsSettlement) {
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> param = new ArrayList<Object>();
        SQL countsql = new SQL();

        SQL sql = new SQL();
        sql.select().cols(
                "achievements_settlement_id", "to_char(start_time, 'YYYY-MM-DD HH24:MI:SS') as start_time", "to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time"
                , "state", "description"
        )
                .from().t("achievements_settlement");
        sql.where(true);
        if (Util.isNotEmpty(form.get("state"))) {
            sql.and(new SqlParm("state").eq().v("?"));
            param.add(Integer.parseInt(form.get("state")));
        }

        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("achievements_settlement_id").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());

    }
}
