package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "member_order")
public class MemberOrder {
    @Id(type = IdType.Seq)
    private Long orderId;
    private Long personId;
    private Long memberProductId;
    private String payCredential;
    private java.sql.Timestamp createTime;
    private Integer orderState;
    private Long auditPerson;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getMemberProductId() {
        return memberProductId;
    }

    public void setMemberProductId(Long memberProductId) {
        this.memberProductId = memberProductId;
    }


    public String getPayCredential() {
        return payCredential;
    }

    public void setPayCredential(String payCredential) {
        this.payCredential = payCredential;
    }


    public java.sql.Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.sql.Timestamp createTime) {
        this.createTime = createTime;
    }


    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }


    public Long getAuditPerson() {
        return auditPerson;
    }

    public void setAuditPerson(Long auditPerson) {
        this.auditPerson = auditPerson;
    }

}
