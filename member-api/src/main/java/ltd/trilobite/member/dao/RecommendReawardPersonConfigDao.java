package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.RecommendReawardPersonConfig;

public class RecommendReawardPersonConfigDao extends BaseDao<RecommendReawardPersonConfig> {
    public RecommendReawardPersonConfig findById(Long id) {
        RecommendReawardPersonConfig config =  new RecommendReawardPersonConfig();
        config.setRecommendReawardPersonConfigId(id);
        return findOne(config, RecommendReawardPersonConfig.class);
    }
}