package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.Withdraw;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class WithdrawDao extends BaseDao<Withdraw> {
    //已完成
    private static final int STATE_COMPLETE = 3;

    public Result findMyNaviList(RestForm form, Long personId) {
//        select w.withdraw_id,w.num,w.state, w.message, w.bank_name, w.bank_num, w.bank_account,p.name as audit_person_name from
//        (
//                select
//                withdraw_id,num,state, audit_person, message, bank_name, bank_num, bank_account,
//                to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,
//                to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time
//                from withdraw
//                where person_id=?
//        ) w
//        left join person p on p.person_id=w.audit_person
//        order by w.create_time desc
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        params.add(personId);
        SQL subsql = new SQL();
        subsql.select().cols("withdraw_id,num,state,person_id, audit_person, message, bank_name, bank_num, bank_account,is_success",
                "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time",
                "to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time" )
                .from().t("withdraw").where().fix("person_id=?");
        SQL sql = new SQL();
        sql.select().cols("w.*,p1.name as person_name,p2.name as audit_person_name")
                .from().subsql(subsql, "w")
                .leftJoin().fix("person p1 on p1.person_id=w.person_id")
                .leftJoin().fix("person p2 on p2.person_id=w.audit_person");
        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("w.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray());
    }

    public Result findNaviList(RestForm form) {
//        select w.*,p1.name as person_name,p2.name as audit_person_name from
//                (
//                        select
//                        withdraw_id,num,state, person_id, audit_person, message, bank_name, bank_num, bank_account,
//                        to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,
//                        to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time
//                        from withdraw
//                        where state=1 and bank_name=? and bank_num=? and bank_account=?
//                ) w
//        left join person p1 on p1.person_id=w.person_id
//        left join person p2 on p2.person_id=w.audit_person
//        order by w.create_time desc
        JdbcTemplet jdbcTemplet = App.get("master");
        List<Object> params = new ArrayList<>();
        SQL subsql = new SQL();
        subsql.select().cols("withdraw_id,num,state, person_id, audit_person, message, bank_name, bank_num, bank_account, is_success",
                        "to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time",
                        "to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time" ).from().t("withdraw").where(true);



        if(StringUtils.isNotBlank(form.get("state"))){
            subsql.fix(" and state=?");
            params.add(Integer.parseInt(form.get("state")));
        }

        if(StringUtils.isNotBlank(form.get("bankAccount"))){
            subsql.fix(" and bank_account=?");
            params.add(StringUtils.trim(form.get("bankAccount")));
        }

        if(StringUtils.isNotBlank(form.get("bankNum"))){
            subsql.fix(" and bank_num=?");
            params.add(StringUtils.trim(form.get("bankNum")));
        }

        SQL sql = new SQL();
        sql.select().cols("w.*,p1.name as person_name,p2.name as audit_person_name,p1.phone_num,c.real_name,c.card_no")
                .from().subsql(subsql, "w")
                .leftJoin().fix("person p1 on p1.person_id=w.person_id")
                .leftJoin().fix("person p2 on p2.person_id=w.audit_person")
                .leftJoin().fix("person_certification c on c.person_id=w.person_id")
                .where(true);

        if(StringUtils.isNotBlank(form.get("personName"))){
            sql.fix(" and p1.name=?");
            params.add(StringUtils.trim(form.get("personName")));
        }

        if(StringUtils.isNotBlank(form.get("phoneNum"))){
            sql.fix(" and p1.phone_num=?");
            params.add(StringUtils.trim(form.get("phoneNum")));
        }

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        sql.order("w.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, params.toArray());

    }

    public Withdraw findById(Long withdrawId) {
        Withdraw withdraw = new Withdraw();
        withdraw.setWithdrawId(withdrawId);
        return findOne(withdraw, Withdraw.class);
    }

    /**
     * 更新状态
     * @param withdrawId
     * @param state
     */
    public void updateState(Long withdrawId, Integer state, long auditPerson) {
        JdbcTemplet jdbcTemplet = App.get("master");
        if (state == STATE_COMPLETE) {
            jdbcTemplet.execute("update withdraw set state=?, is_success=1, audit_person=?, end_time=now() where withdraw_id=?", state, auditPerson, withdrawId);
        }else{
            jdbcTemplet.execute("update withdraw set state=?, audit_person=? where withdraw_id=?", state, auditPerson, withdrawId);
        }
    }

    /**
     * 更新是否成功状态
     * @param withdrawId
     * @param isSuccess
     */
    public void updateIsSuccess(Long withdrawId, Integer isSuccess, String message) {
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("update withdraw set is_success=?, message=? where withdraw_id=?", isSuccess, message, withdrawId);
    }
}
