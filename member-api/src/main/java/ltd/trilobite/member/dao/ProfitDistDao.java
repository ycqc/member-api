package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.ProfitDist;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfitDistDao extends BaseDao<ProfitDist> {

    BillDao billDao = new BillDao();

    public Result naviList(ProfitDist profitDist, RestForm from) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL countsql = new SQL();

        SQL profitDistSql = new SQL();
        profitDistSql.select().cols("profit_dist_id",
                "state", "total", "description",
                "to_char(start_time, 'YYYY-MM-DD HH24:MI:SS') as start_time",
                "to_char(end_time, 'YYYY-MM-DD HH24:MI:SS') as end_time",
                "to_char(m_start_time, 'YYYY-MM-DD HH24:MI:SS') as m_start_time",
                "to_char(m_end_time, 'YYYY-MM-DD HH24:MI:SS') as m_end_time"
        )
                .from().t("profit_dist").where(true);

        List<Object> param = new ArrayList<Object>();
        if (Util.isNotEmpty(profitDist.getState())) {

            profitDistSql.and(new SqlParm("state").eq().v("?"));
            param.add(profitDist.getState());

        }

        countsql.select().cols("count(1)").from(profitDistSql.toString(), "a");

        profitDistSql.order("end_time").desc();
        profitDistSql.limit(from.get("start"), from.get("pageSize"));

        return jdbcTemplet.naviList(profitDistSql.toString(), countsql.toString(), null, param.toArray());
    }

    public Result naviDetialList(ProfitDist profitDist, RestForm from) {
        JdbcTemplet jdbcTemplet = App.get("master");
        ProfitDist profitDistResult = jdbcTemplet.one(profitDist, ProfitDist.class);


        //总数量
        Double totalNumber = jdbcTemplet.getObject("select sum(num) as num from bill where code='3' and create_time>? and create_time<?",
                BigDecimal.class, profitDistResult.getMStartTime(), profitDistResult.getMEndTime()).doubleValue();

        Double onPartAmout = profitDistResult.getTotal() / totalNumber;

        SQL countsql = new SQL();

        //select p.name,p.phone_num,p.gender,b.num,b.num*102.56 as amount,pfit.total from (
        //              select person_id,sum(num) as num from bill where code='3' and create_time>pfit.m_start_time and create_time<pfit.m_end_time
        //              group by  person_id
        //) b left join person p on b.person_id=p.person_id order by p.person_id

        SQL billsql = new SQL();
        billsql.select().cols(" person_id", "sum(num) as num").from().t("bill")
                .where().add(true, "code", "'3'")
                .and(new SqlParm("create_time").gt().v("?"))
                .and(new SqlParm("create_time").lt().v("?"))
                .fix(" group by person_id");

        SQL sql = new SQL();
        sql.select().cols("p.name", "p.phone_num", "p.gender", "b.num", "b.num*" + onPartAmout + " as amount").from(billsql.toString(), "b")
                .leftJoin().t("person p")
                .on(new SqlParm("b.person_id").eq().v("p.person_id"));


        List<Object> param = new ArrayList<Object>();

        param.add(profitDistResult.getMStartTime());
        param.add(profitDistResult.getMEndTime());


        countsql.select().cols("count(1)").from(billsql.toString(), "a");
        sql.order("p.person_id").desc();
        sql.limit(from.get("start"), from.get("pageSize"));

        Result result = new Result(200);
        Map<String, Object> map = new HashMap<>();
        map.put("total_number", totalNumber);
        map.put("total", profitDistResult.getTotal());
        map.put("onepart_amout", onPartAmout);
        map.put("result", jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray()));
        result.setData(map);


        return result;
    }


    public void insertBill(ProfitDist profitDist) {
        JdbcTemplet jdbcTemplet = App.get("master");
        ProfitDist profitDistResult = jdbcTemplet.one(profitDist, ProfitDist.class);

        billDao.delActionSrcId(profitDist.getProfitDistId(), "5");
        //总数量
        Double totalNumber = jdbcTemplet.getObject("select sum(num) as num from bill where code='3' and create_time>? and create_time<?",
                BigDecimal.class, profitDistResult.getMStartTime(), profitDistResult.getMEndTime()).doubleValue();

        Double onPartAmout = profitDistResult.getTotal() / totalNumber;
        SQL billsql = new SQL();
        billsql.select().cols("nextval('bill_seq') as bill_id", " person_id", "sum(num)*" + onPartAmout + " as num", "3 as state", "'5' as code", "'5' as action_code", profitDist.getProfitDistId() + " as src_id", "now() as create_time",
                "'" + profitDistResult.getDescription() + "' as description"
        ).from().t("bill")
                .where().add(true, "code", "'3'")
                .and(new SqlParm("create_time").gt().v("?"))
                .and(new SqlParm("create_time").lt().v("?"))
                .fix(" group by person_id");

        SQL sql = new SQL();
        sql.insert().t("bill").cols(true, "bill_id", "person_id", "num", "state", "code", "action_code", "src_id", "create_time", "description")
                .select().cols("*").from(billsql.toString(), "a");

        jdbcTemplet.execute(sql.toString(), profitDistResult.getMStartTime(), profitDistResult.getMEndTime());

    }


}
