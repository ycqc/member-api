package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.RecommendRewardConfig;

public class RecommendRewardConfigDao extends BaseDao<RecommendRewardConfig>{
    public RecommendRewardConfig findById(Long id) {
        RecommendRewardConfig config =  new RecommendRewardConfig();
        config.setRecommendRewardConfigId(id);
        return findOne(config, RecommendRewardConfig.class);
    }
}
