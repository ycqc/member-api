package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.PersonFunction;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

public class PersonFunctionDao extends BaseDao<PersonFunction>{
    public void remove(PersonFunction personFunction) {
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute("delete from person_function where person_id=? and function_id=?"
                , personFunction.getPersonId(), personFunction.getFunctionId());
    }
}
