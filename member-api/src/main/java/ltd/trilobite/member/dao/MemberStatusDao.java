package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.MemberStatus;
import ltd.trilobite.member.dao.entry.PersonLayer;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MemberStatusDao extends BaseDao<MemberStatus> {

    public Result findSubMember(Long personId, RestForm form) {
//        select p.name,p.gender,p.phone_num,p.member_type,to_char(p.create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,p.tream_num,p.tream_park,p.head_image_url
//        from (select member_id,parent_id  from member_status where parent_id=2) as a
//        left join person p on a.member_id=p.person_id
//        order by p.create_time desc
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select p.name,p.gender,p.phone_num,p.member_type,to_char(p.create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,p.tream_num,p.tream_park,p.head_image_url from (select member_id  from member_status where parent_id=" + personId + ") as a left join person p on a.member_id=p.person_id order by p.create_time desc limit " + form.get("pageSize") + " offset " + form.get("start");
        String countSql = "select count(1)  from member_status where parent_id=" + personId;
        List<Object> param = new ArrayList<Object>();
        return jdbcTemplet.naviList(sql, countSql, null, param.toArray());
    }

    public Map<String, Object> findRecommendPerson(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select p.name,p.gender,p.phone_num,p.member_type,to_char(p.create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time,p.tream_num,p.tream_park,p.head_image_url from person where person_id=(select parent_id from member_status where member_id=?)";
        return jdbcTemplet.getMap(sql, personId);
    }


    public List<PersonLayer> layerParent(PersonLayer personLayer) {
        String sql = "WITH RECURSIVE r AS (\n" +
                "\n" +
                "    SELECT *,1 as depth FROM member_status WHERE member_id= ?\n" +
                "        union   ALL\n" +
                "        SELECT t.*,r.depth+1 as depth FROM member_status t, r WHERE t.member_id= r.parent_id\n" +
                "    )\n" +
                "\n" +
                "SELECT * FROM r where r.depth<=? ORDER BY depth";
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.findList(sql, PersonLayer.class, personLayer.getMemberId(), personLayer.getDepth());

    }

    public List<PersonLayer> layerParentNoDepth(PersonLayer personLayer) {
        String sql = "WITH RECURSIVE r AS (\n" +
                "\n" +
                "    SELECT *,1 as depth FROM member_status WHERE member_id= ?\n" +
                "        union   ALL\n" +
                "        SELECT t.*,r.depth+1 as depth FROM member_status t, r WHERE t.member_id= r.parent_id\n" +
                "    )\n" +
                "\n" +
                "SELECT * FROM r ORDER BY depth";
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.findList(sql, PersonLayer.class, personLayer.getMemberId());

    }
}
