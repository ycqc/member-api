package ltd.trilobite.member.dao.entry;

import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "person_get_goods")
public class PersonGetGoods {

    private Integer personGetGoodsId;
    private Long personId;
    private Long giveGoodsConfigId;
    private Integer isGet;


    public Integer getPersonGetGoodsId() {
        return personGetGoodsId;
    }

    public void setPersonGetGoodsId(Integer personGetGoodsId) {
        this.personGetGoodsId = personGetGoodsId;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    public Long getGiveGoodsConfigId() {
        return giveGoodsConfigId;
    }

    public void setGiveGoodsConfigId(Long giveGoodsConfigId) {
        this.giveGoodsConfigId = giveGoodsConfigId;
    }


    public Integer getIsGet() {
        return isGet;
    }

    public void setIsGet(Integer isGet) {
        this.isGet = isGet;
    }

}
