package ltd.trilobite.member.dao;

import ltd.trilobite.member.dao.entry.MemberOrder;
import ltd.trilobite.member.dao.entry.Person;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.SQL;
import ltd.trilobite.sdk.jdbc.SqlParm;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MemberOrderDao extends BaseDao<MemberOrder> {
    public Result findmyNaviList(Long personId, MemberOrder order, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL memberOrderSql = new SQL();
        memberOrderSql.select().cols("*").from().t("member_order").where().add(true, "person_id", personId.toString());

        List<Object> param = new ArrayList<Object>();
        if (Util.isNotEmpty(order.getOrderState())) {
            memberOrderSql.and(new SqlParm("order_state").eq().v("?"));
            param.add(order.getOrderState());
        }


        SQL memberProductSql = new SQL();
        memberProductSql.select().cols("*").from().t("member_product");
        SQL sql = new SQL();
        sql.select().cols(
                "mo.order_id", "mo.pay_credential", "to_char(mo.create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time", "mo.order_state", "","mp.name", "mp.price"
        ).from(memberOrderSql.toString(), "mo")
                .leftJoin().subsql(memberProductSql, "mp")
                .on(new SqlParm("mo.member_product_id").eq().v("mp.member_product_id"))
                .where(true);
        sql.order("mo.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(memberOrderSql.toString(), "a");

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());

    }

    public Result findNaviList(MemberOrder order, Person person, RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL memberOrderSql = new SQL();
        memberOrderSql.select().cols("*").from().t("member_order").where(true);

        SQL personSql = new SQL();
        personSql.select().cols("person_id", "name", "phone_num", "gender").from().t("person").where(true);


        List<Object> param = new ArrayList<Object>();
        if (Util.isNotEmpty(order.getOrderState())) {
            memberOrderSql.and(new SqlParm("order_state").eq().v("?"));
            param.add(order.getOrderState());
        }

        SQL memberProductSql = new SQL();
        memberProductSql.select().cols("*").from().t("member_product");
        SQL sql = new SQL();
        sql.select().cols(
                "mo.order_id", "mo.pay_credential", "to_char(mo.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time", "mo.order_state", "mp.name as pname", "mp.price"
                , "p.name", "p.phone_num", "p.gender"
        ).from(memberOrderSql.toString(), "mo")
                .leftJoin().subsql(memberProductSql, "mp")
                .on(new SqlParm("mo.member_product_id").eq().v("mp.member_product_id"))
                .leftJoin().subsql(personSql, "p")
                .on(new SqlParm("mo.person_id").eq().v("p.person_id"))
                .where(true);

        if (Util.isNotEmpty(person.getName())) {
            sql.and(new SqlParm("p.name").like().v("?"));
            param.add("%" + person.getName() + "%");
        }

        if (Util.isNotEmpty(person.getPhoneNum())) {
            sql.and(new SqlParm("p.phone_num").eq().v("?"));
            param.add(person.getPhoneNum());
        }

        if (Util.isNotEmpty(person.getGender())) {
            sql.and(new SqlParm("p.gender").eq().v("?"));
            param.add(person.getGender());
        }

        sql.order("mo.create_time").desc();
        sql.limit(form.get("start"), form.get("pageSize"));

        SQL countsql = new SQL();
        countsql.select().cols("count(1)").from(sql.toString(), "a");

        return jdbcTemplet.naviList(sql.toString(), countsql.toString(), null, param.toArray());
    }

    public Map<String, Object> findInfo(MemberOrder memberOrder) {
        JdbcTemplet jdbcTemplet = App.get("master");

        SQL memberOrderSql = new SQL();
        memberOrderSql.select().cols("*").from().t("member_order").where().add(true, "order_id", memberOrder.getOrderId().toString());
        SQL memberProductSql = new SQL();
        memberProductSql.select().cols("*").from().t("member_product");
        SQL sql = new SQL();
        sql.select().cols(
                "mo.order_id", "mo.pay_credential", "to_char(mo.create_time, 'YYYY-MM-DD HH12:MI:SS') as create_time", "mo.order_state", "mp.name", "mp.price"
        ).from(memberOrderSql.toString(), "mo")
                .leftJoin().subsql(memberProductSql, "mp")
                .on(new SqlParm("mo.member_product_id").eq().v("mp.member_product_id"))
                .where(true);
        return jdbcTemplet.getMap(sql.toString());
    }
}
