package ltd.trilobite.member.common;

/**
 * 实名认证常量
 */
public interface CertificationConstant {
    //审核状态
    /**
     * 待审核
     */
    int STATE_UNCHECK = 1;

    /**
     * 审核中
     */
    int STATE_CHECKING = 2;

    /**
     * 已审核
     */
    int STATE_CHECKED = 3;

    //是否审核通过
    /**
     * 默认值
     */
    int AUDIT_DEFAULT = 0;

    /**
     * 通过
     */
    int AUDIT_OK = 1;

    /**
     * 未通过
     */
    int AUDIT_FAIL = 2;
}